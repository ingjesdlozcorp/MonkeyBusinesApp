//
//  RegisterViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 11/06/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import MobileCoreServices

class RegisterViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    var newMedia: Bool?
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userAge: UITextField!
    @IBOutlet weak var maleGender: UISwitch!
    @IBOutlet weak var femaleGender: UISwitch!
    
    //var monkey_users = [NSManagedObject]()
    
    var userGender : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func useCamera(_ sender: AnyObject) {
        
        if UIImagePickerController.isSourceTypeAvailable(
            UIImagePickerController.SourceType.camera) {
                
                let imagePicker = UIImagePickerController()
                
                imagePicker.delegate = self
                imagePicker.sourceType =
                    UIImagePickerController.SourceType.camera
                imagePicker.mediaTypes = [kUTTypeImage as NSString as String]
                imagePicker.allowsEditing = false
                
                self.present(imagePicker, animated: true,
                    completion: nil)
                newMedia = true
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
// Local variable inserted by Swift 4.2 migrator.
let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)

        
        let mediaType = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaType)] as! NSString
        
        self.dismiss(animated: true, completion: nil)
        
        if mediaType.isEqual(to: kUTTypeImage as NSString as String) {
            let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as! UIImage
            
            imageView.image = image
            imagePerfil = imageView.image
            
            self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2
            self.imageView.clipsToBounds = true
            
            if (newMedia == true) {
                //UIImageWriteToSavedPhotosAlbum(image, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
                
                let selectorToCall = Selector(("imageWasSavedSuccessfully:didFinishSavingWithError:context:"))
                UIImageWriteToSavedPhotosAlbum(image, self, selectorToCall, nil)
                
            } else if mediaType.isEqual(to: kUTTypeMovie as String) {
                // Code to support video here
            }
            
        }
        else{
            imageView.image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func image(image:UIImage,error:NSError?,contextInfo:UnsafeRawPointer) {
    
    //@objc func image(_ image: UIImage, didFinishSavingWithError error: NSErrorPointer?, contextInfo:UnsafeRawPointer) {
        
        if error != nil {
            let alert = UIAlertController(title: "Save Failed",
                message: "Failed to save image",
                preferredStyle: UIAlertController.Style.alert)
            
            let cancelAction = UIAlertAction(title: "OK",
                style: .cancel, handler: nil)
            
            alert.addAction(cancelAction)
            self.present(alert, animated: true,
                completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func ActionRegister(_ sender: UIButton) {
        let username = userName.text
        let userage = userAge.text
        let usergender = userGender
        
        //Check for empty fields
        if(username!.isEmpty || userage!.isEmpty || (usergender?.isEmpty)!){
            //Display an alert message
            displayAlertMessage("All fields are required!")
            return
        }
        
        namePerfil = username as NSString?
        
        //Store  UserData local (.sqlite) CoreData
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext!
        
        // Registrar localmente el Usuario en sqlite
        let newUser = NSEntityDescription.insertNewObject(forEntityName: "Users", into: context)
        newUser.setValue(username, forKey: "username")
        newUser.setValue(Int(userage!), forKey: "age")
        newUser.setValue(usergender, forKey: "gender")
        newUser.setValue(Userstars, forKey: "stars")
        
        if((imagePerfil) != nil){
            var imageData : Data!
            imageData = imagePerfil.pngData()
            newUser.setValue(imageData, forKey: "picture")
        }
        else{
            var imageData : Data!
            imageData = UIImage(named: "btn_giraffe_on.png")!.pngData()
            newUser.setValue(imageData, forKey: "picture")
        }
        do {
            try context.save()
        } catch _ {
        }
        
        NotificationCenter.default.post(name: Notification.Name(rawValue: "load"), object: nil)
        
        displayAlertMessage("Hello \(String(describing: namePerfil)). Welcome to MonkeyBusiness App")
        
        //Store data MySQL Database  HTTP Request
        /*
        let myURL = NSURL(string: "http://localhost/MonkeyBusiness/userRegister.php")
        let request = NSMutableURLRequest(URL: myURL!)
        request.HTTPMethod = "POST"
        
        let postString = "username=\(username)&userage=\(userage)&usergender=\(usergender)&userpicture=\(userpicture)"
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request){
            data, response, error in
            
            if(error != nil){
                println("error=\(error)")
                return
            }
            
            var err: NSError?
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers, error: &err) as? NSDictionary
            
            if let parseJSON = json {
                var resultValue = parseJSON["status"] as? String
                println("result: \(resultValue)")
                
                //Registrando al nuevo usuario
                var isUserRegistered:Bool = false
                if(resultValue=="Success"){
                    isUserRegistered = true
                }
                
                var messageToDisplay = parseJSON["message"] as! String!
                
                //El usuario ya estaba registrado
                if(!isUserRegistered){
                    messageToDisplay = parseJSON["message"] as! String!
                }
                
                dispatch_async(dispatch_get_main_queue(), {
                   
                    //Display alert message confirmation
                    var myAlert = UIAlertController(title: "Alert", message:messageToDisplay, preferredStyle: UIAlertControllerStyle.Alert)
                    
                    let oldAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default){ action in
                        self.dismissViewControllerAnimated(true, completion: nil)
                    }
                    
                    myAlert.addAction(oldAction)
                    self.presentViewController(myAlert, animated: true, completion: nil)
                    
                });
            }
        }
        task.resume()
        */
    }
    
    @IBAction func maleValueChanged(_ sender: AnyObject) {
        if maleGender.isOn {
            print("Male is on")
            userGender = "Male"
            maleGender.setOn(true, animated:true)
            femaleGender.setOn(false, animated:true)
        } else {
            print("Male is off")
            maleGender.setOn(false, animated:true)
        }
    }
    
    @IBAction func femaleValueChanged(_ sender: AnyObject) {
        if femaleGender.isOn {
            print("Female is on")
            userGender = "Female"
            femaleGender.setOn(true, animated:true)
            maleGender.setOn(false, animated:true)
        } else {
            print("Female is off")
            femaleGender.setOn(false, animated:true)
        }
    }
    
    func displayAlertMessage(_ messageToDisplay:String){
        //Display alert message confirmation
        let myAlert = UIAlertController(title: "Alert", message:messageToDisplay, preferredStyle: UIAlertController.Style.alert)
        
        let oldAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default){ action in
            self.dismiss(animated: true, completion: nil)
        }
        
        myAlert.addAction(oldAction)
        self.present(myAlert, animated: true, completion: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}

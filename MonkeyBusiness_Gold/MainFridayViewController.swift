//
//  MainFridayViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 26/03/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//


import UIKit
import AVFoundation
import MediaPlayer

class MainFridayViewController: UIViewController, UIPageViewControllerDataSource{
    //weak var delegate : PlayerDelegate?
    
    // MARK: - Variables
    fileprivate var pageViewController: UIPageViewController?
    
    // Initialize it scrollimages
    fileprivate let contentImages = ["",
        "",
        "",
        "",
        "",
        "",
        ""];
    
    // Initialize it right away here
    fileprivate let centerImages = ["6-1a.png",
        "6-1b.png",
        "6-2.png",
        "6-3.png",
        "6-4.png",
        "6-5a.png",
        "6-5b.png"];
    
    
    // Initialize Labels Text
    fileprivate let textLabels = ["Monkey Louie says: “Hello Mr. Elephant, can you tell me what day did your food disappear?” —-“Sure, it was last Friday” Elephant says:",
        "“I remember it was a very hot night, I was showering myself with my trunk, then I turned and I realized my food was gone”",
        "“So do you shower everyday?” —Asks Monkey Louie— “Well, Monkey”—Replies Mr. Elephant—“I don’t see how my showering routine is important to this investigation, but if you must know I shower every two days”",
        "“Hmmm, interesting” says Monkey Louie— “do you remember anything else?” “I remember everything,” says Elephant: “you know, Elephants have excellent memory”",
        "“I do remember something unusual that happened that day,”— he added —I saw a small animal jumping over the fence, but it was too dark, I couldn’t say what animal it was”",
        "Monkey Louie says: “Maybe it was another elephant jumping over your fence?”",
        "—and Elephant replies “That’s not funny monkey, maybe you want me to try jumping over you”"];
    
    // Initialize it with sounds
    fileprivate let soundText = ["6.1A",
        "6.1B",
        "6.2",
        "6.3",
        "6.4",
        "6.5A",
        "6.5B"];
    
    var timer = Timer()
    
    // MARK: - View Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        createPageViewController()
        setupPageControl()
        audiotermino = false
        contador = 0
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(MainFridayViewController.AudioFinished), userInfo: nil, repeats: true)
    }
    
    fileprivate func createPageViewController() {
        let pageController = self.storyboard!.instantiateViewController(withIdentifier: "PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if contentImages.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChild(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
    }
    
    fileprivate func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.backgroundColor = UIColor.clear
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.white
    }
    
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex > 0 {
            ultimo = false
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex+1 < contentImages.count {
            return getItemController(itemController.itemIndex+1)
        }
        else if itemController.itemIndex+1 == contentImages.count{
            ultimo = true
        }
        
        return nil
    }
    
    fileprivate func getItemController(_ itemIndex: Int) -> PageItemController? {
        
        if itemIndex < contentImages.count {
            
            let pageItemController = self.storyboard!.instantiateViewController(withIdentifier: "ItemController") as! PageItemController
            
            pageItemController.itemIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.imageCenter = centerImages[itemIndex]
            pageItemController.ReadText = textLabels[itemIndex]
            pageItemController.audioText = soundText[itemIndex]
            pageItemController.AppearText = textLabels[itemIndex]
            
            actual_index = itemIndex
            
            return pageItemController
        }
        
        return nil
    }
    
    @objc func AudioFinished(){
        if(historyPlayer.isPlaying == false){
            contador += 1
        }
        if(contador>=6 && type_of_read == "autoplay" && standby == false){
            contador = 0
            if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![0])!) {
                
                pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                
                _ = self.children.last
            }
            if(ultimo==true){
                day = "Saturday"
                let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
                self.present(fvc!, animated: false, completion: nil)
                timer.invalidate()
            }
        }
        
    }
    
    @IBAction func ActionNext(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        }
    }
    
    @IBAction func ActionPrev(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
        }
    }
    
    // MARK: - Page Indicator
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return contentImages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }
    
}

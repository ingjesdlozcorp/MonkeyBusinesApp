//
//  UsersViewCell.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 25/06/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit

class UsersViewCell: UICollectionViewCell {
    
    @IBOutlet weak var userText: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    /*@IBOutlet weak var btnDelete: UIButton!
    @IBOutlet weak var btnEdit: UIButton!*/
    
}

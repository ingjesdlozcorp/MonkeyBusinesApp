//
//  MenuViewController.swift
//  MonkeyBusiness_Gold
//
//  Created by Neymar Contreras on 25/02/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class MainMenuViewController: UIViewController {
    @IBOutlet var menu: UIImageView!
    
    @IBOutlet weak var aux: UIImageView!
    @IBOutlet var bg_scroll: UIImageView!
    @IBOutlet var text_read: UIImageView!
    @IBOutlet var text_auto: UIImageView!
    @IBOutlet weak var btn_home: UIButton!
    @IBOutlet weak var btn_home_On: UIButton!
    
    @IBOutlet var btn_open_menu: UIButton!
    @IBOutlet var btn_up_menu: UIButton!
    @IBOutlet var btn_close_menu: UIButton!
    @IBOutlet weak var btn_close_On: UIButton!
    
    @IBOutlet var btn_activityA: UIButton!
    @IBOutlet var btn_activityB: UIButton!
    @IBOutlet var btn_activityC: UIButton!
    @IBOutlet var btn_activityD: UIButton!
    @IBOutlet var btn_activityE: UIButton!
    @IBOutlet var aro_activityA: UIImageView!
    @IBOutlet var aro_activityB: UIImageView!
    @IBOutlet var aro_activityC: UIImageView!
    @IBOutlet var aro_activityD: UIImageView!
    @IBOutlet var aro_activityE: UIImageView!
    
    //@IBOutlet weak var btn_Sunday: UIButton!
    let btn_day = UIButton(type: UIButton.ButtonType.custom)
    @IBOutlet weak var btn_Sunday: UIButton!
    @IBOutlet weak var btn_Sunday_On: UIButton!
    @IBOutlet weak var btn_Monday: UIButton!
    @IBOutlet weak var btn_Monday_On: UIButton!
    @IBOutlet weak var btn_Tuesday: UIButton!
    @IBOutlet weak var btn_Tuesday_On: UIButton!
    @IBOutlet weak var btn_Wednesday: UIButton!
    @IBOutlet weak var btn_Wednesday_On: UIButton!
    @IBOutlet weak var btn_Thursday: UIButton!
    @IBOutlet weak var btn_Thursday_On: UIButton!
    @IBOutlet weak var btn_Friday: UIButton!
    @IBOutlet weak var btn_Friday_On: UIButton!
    @IBOutlet weak var btn_Saturday: UIButton!
    @IBOutlet weak var btn_Saturday_On: UIButton!
    
    @IBOutlet weak var btn_Home: UIButton!
    
    @IBOutlet weak var btn_AP_menu: UIButton!
    @IBOutlet weak var btn_RMS_menu: UIButton!
    
    @IBOutlet weak var btn_RMS_menu_On: UIButton!
    @IBOutlet weak var btn_AP_menu_On: UIButton!
    
    @IBOutlet weak var btn_boston_zoo: UIButton!
    @IBOutlet weak var btn_story_map: UIButton!
    @IBOutlet weak var btn_activity_map: UIButton!
    
    @IBOutlet var jalador: UIView!
    @IBOutlet var perfilView: UIImageView!
    @IBOutlet var perfilName: UILabel!
    @IBOutlet var perfilStars: UILabel!
    
    @IBOutlet var profileView: UIView!
    
    var ir : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if(type_of_read == "autoplay"){
            btn_AP_menu.isSelected = true
            btn_AP_menu_On.alpha = 1.0
            btn_AP_menu.alpha = 0.0
            btn_RMS_menu_On.alpha = 0.0
            btn_RMS_menu.alpha = 1.0
        }
        else{
            btn_RMS_menu.isSelected = true
            btn_AP_menu_On.alpha = 0.0
            btn_AP_menu.alpha = 1.0
            btn_RMS_menu_On.alpha = 1.0
            btn_RMS_menu.alpha = 0.0
        }
        
        perfilView.image = imagePerfil
        perfilName.text = namePerfil as? String
        
        perfilView.layer.cornerRadius = perfilView.frame.size.width / 2
        perfilView.clipsToBounds = true
        perfilStars.text = String(Userstars)
        
        let downSwipe = UISwipeGestureRecognizer(target: self, action: #selector(MainMenuViewController.handleSwipes(_:)))
        let upSwipe = UISwipeGestureRecognizer(target: self, action: #selector(MainMenuViewController.handleSwipes(_:)))
        
        downSwipe.direction = .down
        upSwipe.direction = .up
        
        self.jalador.addGestureRecognizer(downSwipe)
        self.jalador.addGestureRecognizer(upSwipe)
        
        ir = ""
        //accionMenu = ""
        //Creamos un timer para llamar a la funcion de los labels tope menu
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MainMenuViewController.revisionLabels), userInfo: nil, repeats: false)
    }
    
    @objc func handleSwipes(_ sender:UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            print("Swipe Down")
            if(btn_open_menu.alpha == 1.0){
                standby = true
                OpenMenu()
            }
        }
        
        if (sender.direction == .up) {
            print("Swipe Up")
            if(btn_up_menu.alpha == 1.0){
                standby = false
                closeMenuHome()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func revisionLabels(){
        _ = self.menu.frame
        if(day == "Sunday"){
            btn_Sunday_On.alpha = 1.0
            btn_Sunday.alpha = 0.0
        }
        if(day == "Monday"){
            btn_Monday_On.alpha = 1.0
            btn_Monday.alpha = 0.0
        }
        if(day == "Tuesday"){
            btn_Tuesday_On.alpha = 1.0
            btn_Tuesday.alpha = 0.0
        }
        if(day == "Wednesday"){
            btn_Wednesday_On.alpha = 1.0
            btn_Wednesday.alpha = 0.0
        }
        if(day == "Thursday"){
            btn_Thursday_On.alpha = 1.0
            btn_Thursday.alpha = 0.0
        }
        if(day == "Friday"){
            btn_Friday_On.alpha = 1.0
            btn_Friday.alpha = 0.0
        }
        if(day == "Saturday"){
            btn_Saturday_On.alpha = 1.0
            btn_Saturday.alpha = 0.0
        }
        if(day == "activityA"){
            aro_activityA.alpha = 1.0
        }
        if(day == "activityB"){
            aro_activityB.alpha = 1.0
        }
        if(day == "activityC"){
            aro_activityC.alpha = 1.0
        }
        if(day == "activityD"){
            aro_activityD.alpha = 1.0
        }
        if(day == "activityE"){
            aro_activityE.alpha = 1.0
        }
    }
    
    
    @objc func GoHome() {
        if(ir == "OpenView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "OpenView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "DaysOfTheWeekView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "DaysOfTheWeekView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "FirstActivityView"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "FirstActivityView")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "MondayRoutine"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "MondayRoutine")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "HowManyDays"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "HowManyDays")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "BeforeAndAfter"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "BeforeAndAfter")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "IntroDay"){
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "SundayView"){
            //let fvc = self.storyboard?.instantiateViewControllerWithIdentifier("SundayView") as! UIViewController
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "MondayView"){
            //let fvc = self.storyboard?.instantiateViewControllerWithIdentifier("MondayView") as! UIViewController
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "TuesdayView"){
            //let fvc = self.storyboard?.instantiateViewControllerWithIdentifier("TuesdayView") as! UIViewController
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "WednesdayView"){
            //let fvc = self.storyboard?.instantiateViewControllerWithIdentifier("WednesdayView") as! UIViewController
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "ThursdayView"){
            //let fvc = self.storyboard?.instantiateViewControllerWithIdentifier("ThursdayView") as! UIViewController
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "FridayView"){
            //let fvc = self.storyboard?.instantiateViewControllerWithIdentifier("FridayView") as! UIViewController
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        if(ir == "SaturdayView"){
            //let fvc = self.storyboard?.instantiateViewControllerWithIdentifier("SaturdayView") as! UIViewController
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
    }
    
    @IBAction func musicSliderValueChanged(_ sender: UISlider) {
        let sliderValue = sender.value
        soundPlayer.volume = sliderValue
    }
    
    func loadIntroView(){
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    func resetButtonsOff(){
        self.btn_Sunday.alpha = 1.0
        self.btn_Monday.alpha = 1.0
        self.btn_Tuesday.alpha = 1.0
        self.btn_Wednesday.alpha = 1.0
        self.btn_Thursday.alpha = 1.0
        self.btn_Friday.alpha = 1.0
        self.btn_Saturday.alpha = 1.0
        self.btn_home_On.alpha = 0.0
        self.btn_close_On.alpha = 0.0
        self.btn_Sunday_On.alpha = 0.0
        self.btn_Monday_On.alpha = 0.0
        self.btn_Tuesday_On.alpha = 0.0
        self.btn_Wednesday_On.alpha = 0.0
        self.btn_Thursday_On.alpha = 0.0
        self.btn_Friday_On.alpha = 0.0
        self.btn_Saturday_On.alpha = 0.0
        self.btn_home_On.alpha = 0.0
        self.btn_close_On.alpha = 0.0
        self.aro_activityA.alpha = 0.0
        self.aro_activityB.alpha = 0.0
        self.aro_activityC.alpha = 0.0
        self.aro_activityD.alpha = 0.0
        self.aro_activityE.alpha = 0.0
    }
    
    @IBAction func ActionReadMyself(_ sender:UIButton!)
    {
        type_of_read = "read_myself"
        btn_AP_menu_On.alpha = 0.0
        btn_AP_menu.alpha = 1.0
        btn_RMS_menu_On.alpha = 1.0
        btn_RMS_menu.alpha = 0.0
        
        let audio_btn = "R-for-Read-Myself"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: audio_btn, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        if(historyPlayer.isPlaying == true){
            historyPlayer.stop()
        }
    }
    
    @IBAction func ActionAutoPlay(_ sender:UIButton!)
    {
        type_of_read = "autoplay"
        btn_AP_menu_On.alpha = 1.0
        btn_AP_menu.alpha = 0.0
        btn_RMS_menu_On.alpha = 0.0
        btn_RMS_menu.alpha = 1.0
        
        let audio_btn = "A-for-Autoplay"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: audio_btn, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }

    @IBAction func buttonHome(_ sender: AnyObject) {
        ir = "OpenView"
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let audio_btn = "home"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: audio_btn, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
        
            soundPlayer.play()
            
            self.btn_home_On.alpha = 1.0
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonSunday(_ sender:UIButton!)
    {
        day = "Sunday"
        ir = "SundayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Giraffe"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Sunday_On.alpha = 1.0
            self.btn_Sunday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }

    @IBAction func buttonMonday(_ sender:UIButton!)
    {
        day = "Monday"
        ir = "MondayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Koala-Bear"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Monday_On.alpha = 1.0
            self.btn_Monday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonTuesday(_ sender:UIButton!)
    {
        day = "Tuesday"
        ir = "TuesdayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Cheetah"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Tuesday_On.alpha = 1.0
            self.btn_Tuesday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonWednesday(_ sender:UIButton!)
    {
        day = "Wednesday"
        ir = "WednesdayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Crocodile"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Wednesday_On.alpha = 1.0
            self.btn_Wednesday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonThursday(_ sender:UIButton!)
    {
        day = "Thursday"
        ir = "ThursdayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Polar-Bear"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Thursday_On.alpha = 1.0
            self.btn_Thursday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonFriday(_ sender:UIButton!)
    {
        day = "Friday"
        ir = "FridayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Elephant"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Friday_On.alpha = 1.0
            self.btn_Friday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    @IBAction func buttonSaturday(_ sender:UIButton!)
    {
        day = "Saturday"
        ir = "SaturdayView"
        resetButtonsOff()
        UIView.animate(withDuration: 1.0, delay: 0.1, options: .curveEaseOut, animations: {
            let sonido_activity = "Monkey-Louie"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                soundPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                soundPlayer = AVAudioPlayer()
            }
            soundPlayer.prepareToPlay()
            
            self.btn_Saturday_On.alpha = 1.0
            self.btn_Saturday.alpha = 0.0
            soundPlayer.play()
            }, completion: { finished in self.closeMenuHome()
        })
    }
    
    //Boton que hace el llamado al Menú Principal
    @IBAction func buttonOpenMenu(_ sender:UIButton!)
    {
        accionMenu = "Abrir Menu"
        if(activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
    }
    
    //Boton que hace el llamado a Cerrar el Menú Principal
    @IBAction func buttonCloseMenu(_ sender:UIButton!)
    {
        accionMenu = "Cerrar Menu"
        let sonido_activity = "Close"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        closeMenuHome()
    }
    
    func closeMenu(){
        ir = "IntroDay"
        accionMenu = "Cerrar Menu"
        
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MainMenuViewController.GoHome), userInfo: nil, repeats: false)
    }

    func OpenMenu()
    {
        if(historyPlayer.isPlaying == true){
            historyPlayer.stop()
        }
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .curveEaseOut, animations: {
            self.btn_open_menu.alpha = 0.0
            self.btn_close_menu.alpha = 1.0
            self.btn_up_menu.alpha = 1.0
            var menu_frame = self.menu.frame
            menu_frame.origin.y += menu_frame.size.height - 275
            var btn_close = self.btn_close_menu.frame
            btn_close.origin.y += menu_frame.size.height - 275
            var btn_close_on = self.btn_close_On.frame
            btn_close_on.origin.y += menu_frame.size.height - 275
            var jalador_down = self.jalador.frame
            jalador_down.origin.y += menu_frame.size.height - 275
            
            var btnActA_frame = self.btn_activityA.frame
            btnActA_frame.origin.y += menu_frame.size.height - 275
            var btnActB_frame = self.btn_activityB.frame
            btnActB_frame.origin.y += menu_frame.size.height - 275
            var btnActC_frame = self.btn_activityC.frame
            btnActC_frame.origin.y += menu_frame.size.height - 275
            var btnActD_frame = self.btn_activityD.frame
            btnActD_frame.origin.y += menu_frame.size.height - 275
            var btnActE_frame = self.btn_activityE.frame
            btnActE_frame.origin.y += menu_frame.size.height - 275
            var btnActA_frame_aro = self.aro_activityA.frame
            btnActA_frame_aro.origin.y += menu_frame.size.height - 275
            var btnActB_frame_aro = self.aro_activityB.frame
            btnActB_frame_aro.origin.y += menu_frame.size.height - 275
            var btnActC_frame_aro = self.aro_activityC.frame
            btnActC_frame_aro.origin.y += menu_frame.size.height - 275
            var btnActD_frame_aro = self.aro_activityD.frame
            btnActD_frame_aro.origin.y += menu_frame.size.height - 275
            var btnActE_frame_aro = self.aro_activityE.frame
            btnActE_frame_aro.origin.y += menu_frame.size.height - 275
            
            var btnHome_frame = self.btn_home.frame
            btnHome_frame.origin.y += menu_frame.size.height - 275
            var btnHome_frame_On = self.btn_home_On.frame
            btnHome_frame_On.origin.y += menu_frame.size.height - 275
            var btn_readmyself_frame = self.btn_RMS_menu.frame
            btn_readmyself_frame.origin.y += menu_frame.size.height - 275
            var btn_readmyself_On_frame = self.btn_RMS_menu_On.frame
            btn_readmyself_On_frame.origin.y += menu_frame.size.height - 275
            var btn_autoplay_frame = self.btn_AP_menu.frame
            btn_autoplay_frame.origin.y += menu_frame.size.height - 275
            var btn_autoplay_On_frame = self.btn_AP_menu_On.frame
            btn_autoplay_On_frame.origin.y += menu_frame.size.height - 275
            
            var btnBostonZoo_frame = self.btn_boston_zoo.frame
            btnBostonZoo_frame.origin.y += menu_frame.size.height - 275
            var btnStoryMap_frame = self.btn_story_map.frame
            btnStoryMap_frame.origin.y += menu_frame.size.height - 275
            var btnActivity_frame = self.btn_activity_map.frame
            btnActivity_frame.origin.y += menu_frame.size.height - 275
            
            var btnSunday_frame = self.btn_Sunday.frame
            btnSunday_frame.origin.y += menu_frame.size.height - 275
            var btnSundayOn_frame = self.btn_Sunday_On.frame
            btnSundayOn_frame.origin.y += menu_frame.size.height - 275
            var btnMonday_frame = self.btn_Monday.frame
            btnMonday_frame.origin.y += menu_frame.size.height - 275
            var btnMondayOn_frame = self.btn_Monday_On.frame
            btnMondayOn_frame.origin.y += menu_frame.size.height - 275
            var btnTuesday_frame = self.btn_Tuesday.frame
            btnTuesday_frame.origin.y += menu_frame.size.height - 275
            var btnTuesdayOn_frame = self.btn_Tuesday_On.frame
            btnTuesdayOn_frame.origin.y += menu_frame.size.height - 275
            var btnWednesday_frame = self.btn_Wednesday.frame
            btnWednesday_frame.origin.y += menu_frame.size.height - 275
            var btnWednesdayOn_frame = self.btn_Wednesday_On.frame
            btnWednesdayOn_frame.origin.y += menu_frame.size.height - 275
            var btnThursday_frame = self.btn_Thursday.frame
            btnThursday_frame.origin.y += menu_frame.size.height - 275
            var btnThursdayOn_frame = self.btn_Thursday_On.frame
            btnThursdayOn_frame.origin.y += menu_frame.size.height - 275
            var btnFriday_frame = self.btn_Friday.frame
            btnFriday_frame.origin.y += menu_frame.size.height - 275
            var btnFridayOn_frame = self.btn_Friday_On.frame
            btnFridayOn_frame.origin.y += menu_frame.size.height - 275
            var btnSaturday_frame = self.btn_Saturday.frame
            btnSaturday_frame.origin.y += menu_frame.size.height - 275
            var btnSaturdayOn_frame = self.btn_Saturday_On.frame
            btnSaturdayOn_frame.origin.y += menu_frame.size.height - 275
            
            var profile_frame = self.profileView.frame
            profile_frame.origin.y += menu_frame.size.height - 275
            var aux_frame = self.aux.frame
                       aux_frame.origin.y += menu_frame.size.height - 275
            
            self.menu.frame = menu_frame
            self.aux.frame = aux_frame
            self.btn_close_menu.frame = btn_close
            self.btn_close_On.frame = btn_close_on
            //self.btn_open_menu.frame = btn_open
            self.jalador.frame = jalador_down
            
            self.btn_activityA.frame = btnActA_frame
            self.btn_activityB.frame = btnActB_frame
            self.btn_activityC.frame = btnActC_frame
            self.btn_activityD.frame = btnActD_frame
            self.btn_activityE.frame = btnActE_frame
            self.aro_activityA.frame = btnActA_frame_aro
            self.aro_activityB.frame = btnActB_frame_aro
            self.aro_activityC.frame = btnActC_frame_aro
            self.aro_activityD.frame = btnActD_frame_aro
            self.aro_activityE.frame = btnActE_frame_aro
            
            self.btn_home.frame = btnHome_frame
            self.btn_home_On.frame = btnHome_frame_On
            self.btn_RMS_menu.frame = btn_readmyself_frame
            self.btn_RMS_menu_On.frame = btn_readmyself_On_frame
            self.btn_AP_menu.frame = btn_autoplay_frame
            self.btn_AP_menu_On.frame = btn_autoplay_On_frame
            
            self.btn_boston_zoo.frame = btnBostonZoo_frame
            self.btn_story_map.frame = btnStoryMap_frame
            self.btn_activity_map.frame = btnActivity_frame
            
            self.btn_Sunday.frame = btnSunday_frame
            self.btn_Sunday_On.frame = btnSundayOn_frame
            self.btn_Monday.frame = btnMonday_frame
            self.btn_Monday_On.frame = btnMondayOn_frame
            self.btn_Tuesday.frame = btnTuesday_frame
            self.btn_Tuesday_On.frame = btnTuesdayOn_frame
            self.btn_Wednesday.frame = btnWednesday_frame
            self.btn_Wednesday_On.frame = btnWednesdayOn_frame
            self.btn_Thursday.frame = btnThursday_frame
            self.btn_Thursday_On.frame = btnThursdayOn_frame
            self.btn_Friday.frame = btnFriday_frame
            self.btn_Friday_On.frame = btnFridayOn_frame
            self.btn_Saturday.frame = btnSaturday_frame
            self.btn_Saturday_On.frame = btnSaturdayOn_frame
            self.profileView.frame = profile_frame
            
            }, completion: nil)
    }

    @objc func closeMenuHome(){
        UIView.animate(withDuration: 0.7, delay: 0.0, options: .curveEaseOut, animations: {
            self.btn_close_menu.alpha = 0.0
            self.btn_open_menu.alpha = 1.0
            self.btn_up_menu.alpha = 0.0
            var menu_frame = self.menu.frame
            menu_frame.origin.y -= menu_frame.size.height - 275
            var btn_close = self.btn_close_menu.frame
            btn_close.origin.y -= menu_frame.size.height - 275
            var jalador_down = self.jalador.frame
            jalador_down.origin.y -= menu_frame.size.height - 275
            
            var btnActA_frame = self.btn_activityA.frame
            btnActA_frame.origin.y -= menu_frame.size.height - 275
            var btnActB_frame = self.btn_activityB.frame
            btnActB_frame.origin.y -= menu_frame.size.height - 275
            var btnActC_frame = self.btn_activityC.frame
            btnActC_frame.origin.y -= menu_frame.size.height - 275
            var btnActD_frame = self.btn_activityD.frame
            btnActD_frame.origin.y -= menu_frame.size.height - 275
            var btnActE_frame = self.btn_activityE.frame
            btnActE_frame.origin.y -= menu_frame.size.height - 275
            var btnActA_frame_aro = self.aro_activityA.frame
            btnActA_frame_aro.origin.y -= menu_frame.size.height - 275
            var btnActB_frame_aro = self.aro_activityB.frame
            btnActB_frame_aro.origin.y -= menu_frame.size.height - 275
            var btnActC_frame_aro = self.aro_activityC.frame
            btnActC_frame_aro.origin.y -= menu_frame.size.height - 275
            var btnActD_frame_aro = self.aro_activityD.frame
            btnActD_frame_aro.origin.y -= menu_frame.size.height - 275
            var btnActE_frame_aro = self.aro_activityE.frame
            btnActE_frame_aro.origin.y -= menu_frame.size.height - 275
            
            var btnHome_frame = self.btn_home.frame
            btnHome_frame.origin.y -= menu_frame.size.height - 275
            var btnHome_frame_On = self.btn_home_On.frame
            btnHome_frame_On.origin.y -= menu_frame.size.height - 275
            var btn_readmyself_frame = self.btn_RMS_menu.frame
            btn_readmyself_frame.origin.y -= menu_frame.size.height - 275
            var btn_readmyself_On_frame = self.btn_RMS_menu_On.frame
            btn_readmyself_On_frame.origin.y -= menu_frame.size.height - 275
            var btn_autoplay_frame = self.btn_AP_menu.frame
            btn_autoplay_frame.origin.y -= menu_frame.size.height - 275
            var btn_autoplay_On_frame = self.btn_AP_menu_On.frame
            btn_autoplay_On_frame.origin.y -= menu_frame.size.height - 275
            
            var btnBostonZoo_frame = self.btn_boston_zoo.frame
            btnBostonZoo_frame.origin.y -= menu_frame.size.height - 275
            var btnStoryMap_frame = self.btn_story_map.frame
            btnStoryMap_frame.origin.y -= menu_frame.size.height - 275
            var btnActivity_frame = self.btn_activity_map.frame
            btnActivity_frame.origin.y -= menu_frame.size.height - 275
            
            var btnSunday_frame = self.btn_Sunday.frame
            btnSunday_frame.origin.y -= menu_frame.size.height - 275
            var btnSundayOn_frame = self.btn_Sunday_On.frame
            btnSundayOn_frame.origin.y -= menu_frame.size.height - 275
            var btnMonday_frame = self.btn_Monday.frame
            btnMonday_frame.origin.y -= menu_frame.size.height - 275
            var btnMondayOn_frame = self.btn_Monday_On.frame
            btnMondayOn_frame.origin.y -= menu_frame.size.height - 275
            var btnTuesday_frame = self.btn_Tuesday.frame
            btnTuesday_frame.origin.y -= menu_frame.size.height - 275
            var btnTuesdayOn_frame = self.btn_Tuesday_On.frame
            btnTuesdayOn_frame.origin.y -= menu_frame.size.height - 275
            var btnWednesday_frame = self.btn_Wednesday.frame
            btnWednesday_frame.origin.y -= menu_frame.size.height - 275
            var btnWednesdayOn_frame = self.btn_Wednesday_On.frame
            btnWednesdayOn_frame.origin.y -= menu_frame.size.height - 275
            var btnThursday_frame = self.btn_Thursday.frame
            btnThursday_frame.origin.y -= menu_frame.size.height - 275
            var btnThursdayOn_frame = self.btn_Thursday_On.frame
            btnThursdayOn_frame.origin.y -= menu_frame.size.height - 275
            var btnFriday_frame = self.btn_Friday.frame
            btnFriday_frame.origin.y -= menu_frame.size.height - 275
            var btnFridayOn_frame = self.btn_Friday_On.frame
            btnFridayOn_frame.origin.y -= menu_frame.size.height - 275
            var btnSaturday_frame = self.btn_Saturday.frame
            btnSaturday_frame.origin.y -= menu_frame.size.height - 275
            var btnSaturdayOn_frame = self.btn_Saturday_On.frame
            btnSaturdayOn_frame.origin.y -= menu_frame.size.height - 275
            
            var profile_frame = self.profileView.frame
            profile_frame.origin.y -= menu_frame.size.height - 275
            var aux_frame = self.aux.frame
                                  aux_frame.origin.y -= menu_frame.size.height - 275
            
            self.menu.frame = menu_frame
            self.aux.frame = aux_frame
            //self.btn_open_menu.frame = btn_open
            self.btn_close_menu.frame = btn_close
            self.jalador.frame = jalador_down
            
            self.btn_activityA.frame = btnActA_frame
            self.btn_activityB.frame = btnActB_frame
            self.btn_activityC.frame = btnActC_frame
            self.btn_activityD.frame = btnActD_frame
            self.btn_activityE.frame = btnActE_frame
            self.aro_activityA.frame = btnActA_frame_aro
            self.aro_activityB.frame = btnActB_frame_aro
            self.aro_activityC.frame = btnActC_frame_aro
            self.aro_activityD.frame = btnActD_frame_aro
            self.aro_activityE.frame = btnActE_frame_aro
            
            self.btn_home.frame = btnHome_frame
            self.btn_home_On.frame = btnHome_frame_On
            self.btn_RMS_menu.frame = btn_readmyself_frame
            self.btn_RMS_menu_On.frame = btn_readmyself_On_frame
            self.btn_AP_menu.frame = btn_autoplay_frame
            self.btn_AP_menu_On.frame = btn_autoplay_On_frame
            
            self.btn_boston_zoo.frame = btnBostonZoo_frame
            self.btn_story_map.frame = btnStoryMap_frame
            self.btn_activity_map.frame = btnActivity_frame
            
            self.btn_Sunday.frame = btnSunday_frame
            self.btn_Sunday_On.frame = btnSundayOn_frame
            self.btn_Monday.frame = btnMonday_frame
            self.btn_Monday_On.frame = btnMondayOn_frame
            self.btn_Tuesday.frame = btnTuesday_frame
            self.btn_Tuesday_On.frame = btnTuesdayOn_frame
            self.btn_Wednesday.frame = btnWednesday_frame
            self.btn_Wednesday_On.frame = btnWednesdayOn_frame
            self.btn_Thursday.frame = btnThursday_frame
            self.btn_Thursday_On.frame = btnThursdayOn_frame
            self.btn_Friday.frame = btnFriday_frame
            self.btn_Friday_On.frame = btnFridayOn_frame
            self.btn_Saturday.frame = btnSaturday_frame
            self.btn_Saturday_On.frame = btnSaturdayOn_frame
            self.profileView.frame = profile_frame
            
            }, completion: { finished in self.GoHome()
        })
    }
    
    @IBAction func buttonActivityA(_ sender:UIButton!)
    {
        resetButtonsOff()
        let sonido_activity = "Counting-Days"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        day = "activityA"
        ir = "DaysOfTheWeekView"
        
        aro_activityA.alpha = 1.0
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuViewController.closeMenuHome), userInfo: nil, repeats: false)
    }
    
    @IBAction func buttonActivityB(_ sender:UIButton!)
    {
        resetButtonsOff()
        let sonido_activity = "Earth-Rotation"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        day = "activityB"
        ir = "FirstActivityView"
        
        aro_activityB.alpha = 1.0
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuViewController.closeMenuHome), userInfo: nil, repeats: false)
    }
    
    @IBAction func buttonActivityC(_ sender:UIButton!)
    {
        resetButtonsOff()
        let sonido_activity = "Daily-Routine"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        day = "activityC"
        ir = "MondayRoutine"
        
        aro_activityC.alpha = 1.0
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuViewController.closeMenuHome), userInfo: nil, repeats: false)
    }
    
    @IBAction func buttonActivityD(_ sender:UIButton!)
    {
        resetButtonsOff()
        let sonido_activity = "How-Many-Times"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        day = "activityD"
        ir = "HowManyDays"
        
        aro_activityD.alpha = 1.0
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuViewController.closeMenuHome), userInfo: nil, repeats: false)
    }
    
    @IBAction func buttonActivityE(_ sender:UIButton!)
    {
        resetButtonsOff()
        let sonido_activity = "Before-and-After"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
        
        day = "activityE"
        ir = "BeforeAndAfter"
        
        aro_activityE.alpha = 1.0
        _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(MainMenuViewController.closeMenuHome), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionBostonZoo(_ sender: AnyObject) {
        let sonido_activity = "Boston_Zoo"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        
        soundPlayer.play()
    }
    
    
    @IBAction func ActionStoryMap(_ sender: AnyObject) {
        let sonido_activity = "Story-Map"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    
    @IBAction func ActionActivityMap(_ sender: AnyObject) {
        let sonido_activity = "ActivityMap"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            soundPlayer = AVAudioPlayer()
        }
        soundPlayer.prepareToPlay()
        soundPlayer.play()
    }
    
    deinit {
        debugPrint("Menu deinitialized...")
    }

}

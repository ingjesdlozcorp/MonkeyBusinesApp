//
//  UsersMonkeyAppViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 25/06/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import CoreData

var imagePerfil : UIImage!
var namePerfil : NSString!

class UsersMonkeyAppViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {

    //var tableData: [String] = ["Emma", "Joha", "Yhilmer"]
    //var tableImages: [String] = ["btn_giraffe_on.png", "btn_giraffe_on.png", "btn_giraffe_on.png"]
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var tableData: [String] = []{
        didSet {
            self.collectionView.reloadData()
        }
    }

    var tableImages: [Data] = []{
        didSet {
            self.collectionView.reloadData()
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(UsersMonkeyAppViewController.loadList(_:)), name:NSNotification.Name(rawValue: "load"), object: nil)
        
        // Do any additional setup after loading the view.
        loadUsers()
    }

    
    @objc func loadList(_ notification: Notification){
        loadRecentUser()
    }
    
    func loadUsers(){
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext!
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.returnsObjectsAsFaults = false
        
        let results:NSArray = try! context.fetch(request) as NSArray
        
        if(results.count > 0){
            for res in results{
                let valor = (res as AnyObject).value(forKey: "username") as? String
                
                var imagenData : Data!
                imagenData = UIImage(data: (res as AnyObject).value(forKey: "picture") as! Data)!.pngData()
                
                tableData.append(valor!)
                tableImages.append(imagenData)
            }
        }
        /*else{
            tableData.append("Jiraffe")
            var imageData : NSData!
            imageData = UIImagePNGRepresentation(UIImage(named: "btn_giraffe_on.png"))
            tableImages.append(imageData)
        }*/
    }
    
    func loadRecentUser(){
        let appDel: AppDelegate = (UIApplication.shared.delegate as! AppDelegate)
        let context: NSManagedObjectContext = appDel.managedObjectContext!
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.returnsObjectsAsFaults = false
        
        let results:NSArray = try! context.fetch(request) as NSArray
        
        if(results.count > 0){
                let ulti = Int(results.count)-1
                let res = results[ulti] as! NSManagedObject
            let valor = res.value(forKey: "username")
            
                var imagenData : Data!
                imagenData = UIImage(data: res.value(forKey: "picture") as! Data)!.pngData()
                
            tableData.append(valor as! String)
                tableImages.append(imagenData)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tableData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UsersViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! UsersViewCell
        cell.userText.text = tableData[indexPath.row]
        cell.userImage.image = UIImage(data: tableImages[indexPath.row] as Data)
        
        cell.userImage.layer.cornerRadius = cell.userImage.frame.size.width / 2
        cell.userImage.clipsToBounds = true
        /*
        cell.btnEdit.addTarget(self, action: "buttonEdit:", forControlEvents:.TouchUpInside)
        cell.btnDelete.addTarget(self, action: "buttonDelete:", forControlEvents:.TouchUpInside)
        */
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Cell \(indexPath.row) selected")
        _ = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! UsersViewCell
        namePerfil = tableData[indexPath.row] as NSString
        imagePerfil = UIImage(data: tableImages[indexPath.row] as Data)
        
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "OpenView")
        self.present(fvc!, animated: false, completion: nil)
    }

    @IBAction func buttonDelete(_ sender:UIButton!)
    {
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "DeleteView")
        self.present(fvc!, animated: false, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

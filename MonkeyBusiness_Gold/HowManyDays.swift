//
//  HowManyDays.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 14/04/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//
import UIKit
import AVFoundation
import MediaPlayer

class HowManyDays: UIViewController {
    @IBOutlet weak var btn_info: UIButton!
    var star : UIImageView!
    
    @IBOutlet weak var btn_one_off: UIButton!
    @IBOutlet weak var btn_two_off: UIButton!
    @IBOutlet weak var btn_three_off: UIButton!
    @IBOutlet weak var btn_one_on: UIButton!
    @IBOutlet weak var btn_two_on: UIButton!
    @IBOutlet weak var btn_three_on: UIButton!
    
    @IBOutlet weak var brushing_teeth: UIImageView!
    @IBOutlet weak var cleaning_ears: UIImageView!
    @IBOutlet weak var taking_shower: UIImageView!
    @IBOutlet weak var eating_banana: UIImageView!
    @IBOutlet weak var removing_pulgas: UIImageView!
    
    @IBOutlet weak var bgIntro: UIView!
    var timer : Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Removed deprecated use of AVAudioSessionDelegate protocol
        //AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
        //AVAudioSession.sharedInstance().setActive(true, error: nil)
        accionMenu = ""
        activity_name = "activityD"
        pausa = false
        
        star = UIImageView()
        star.image = UIImage(named: "star.png")
        //Creamos un timer para llamar a la animación de cargar Lunes
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(HowManyDays.loadIntro), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionInfo(_ sender: AnyObject) {
        timer.invalidate()
        if(activityPlayer.isPlaying){
            activityPlayer.stop()
        }
        pausa = true
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "HowManyDays")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    @objc func loadIntro(){
        let sonido_activity = "Crocodile-says-blackbird-cleans"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(HowManyDays.delay), userInfo: nil, repeats: false)
    }
    
    @objc func delay(){
        if(pausa == false){
            _ = Timer.scheduledTimer(timeInterval: 5.5, target: self, selector: #selector(HowManyDays.LoadImage1), userInfo: nil, repeats: false)
        }
    }
    
    func resetImages(){
        self.brushing_teeth.alpha = 0.0
        self.cleaning_ears.alpha = 0.0
        self.taking_shower.alpha = 0.0
        self.eating_banana.alpha = 0.0
        self.removing_pulgas.alpha = 0.0
        self.btn_one_off.alpha = 0.0
        self.btn_two_off.alpha = 0.0
        self.btn_three_off.alpha = 0.0
        self.btn_one_on.alpha = 0.0
        self.btn_two_on.alpha = 0.0
        self.btn_three_on.alpha = 0.0
    }
    
    @objc func LoadImage1(){
        resetImages()
        if(pausa == false){
            self.bgIntro.alpha = 0.0
            self.brushing_teeth.alpha = 1.0
            let sonido_activity = "M-brushes-his-teeth"
        
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
            _ = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(HowManyDays.delayArrow1), userInfo: nil, repeats: false)
        }
    }
   
    
    @objc func delayArrow1(){
        if(pausa == false){
            let sonido_activity = "One-time-a-day"
        
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
                self.btn_one_off.alpha = 1.0
                }, completion: nil)
            timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(HowManyDays.delayArrow2), userInfo: nil, repeats: false)
        }
    }
    
    @objc func delayArrow2(){
        if(pausa == false){
            let sonido_activity = "2-times-a-day"
        
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
                self.btn_two_off.alpha = 1.0
                }, completion: nil)
            _ = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(HowManyDays.delayArrow3), userInfo: nil, repeats: false)
        }
    }
    
    @objc func delayArrow3(){
        if(pausa == false){
            let sonido_activity = "3-times-a-day"
        
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
                self.btn_three_off.alpha = 1.0
            }, completion: nil)
        }
    }
    
    @objc func LoadImage2(){
        resetImages()
        let sonido_activity = "M-cleans-his-ears"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.cleaning_ears.alpha = 1.0
            self.btn_one_off.alpha = 1.0
            self.btn_two_off.alpha = 1.0
            self.btn_three_off.alpha = 1.0
            }, completion: nil)
    }
    
    @objc func LoadImage3(){
        resetImages()
        let sonido_activity = "M-showers"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.taking_shower.alpha = 1.0
            self.btn_one_off.alpha = 1.0
            self.btn_two_off.alpha = 1.0
            self.btn_three_off.alpha = 1.0
            }, completion: nil)
    }
    
    @objc func LoadImage4(){
        resetImages()
        let sonido_activity = "M-eats"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.eating_banana.alpha = 1.0
            self.btn_one_off.alpha = 1.0
            self.btn_two_off.alpha = 1.0
            self.btn_three_off.alpha = 1.0
            }, completion: nil)
    }
    
    @objc func LoadImage5(){
        resetImages()
        let sonido_activity = "M-remove-his"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.removing_pulgas.alpha = 1.0
            self.btn_one_off.alpha = 1.0
            self.btn_two_off.alpha = 1.0
            self.btn_three_off.alpha = 1.0
            }, completion: nil)
    }
    
    
    @IBAction func OneTime(_ sender: AnyObject) {
        self.btn_one_on.alpha = 1.0
        self.btn_one_off.alpha = 0.0
        let sonido_activity = "One-time-a-day"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        _ = Timer.scheduledTimer(timeInterval: 2.2, target: self, selector: #selector(HowManyDays.rightAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func TwoTimes(_ sender: AnyObject) {
        self.btn_two_on.alpha = 1.0
        self.btn_two_off.alpha = 0.0
        let sonido_activity = "2-times-a-day"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        _ = Timer.scheduledTimer(timeInterval: 2.5, target: self, selector: #selector(HowManyDays.rightAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ThreeTimes(_ sender: AnyObject) {
        self.btn_three_on.alpha = 1.0
        self.btn_three_off.alpha = 0.0
        let sonido_activity = "3-times-a-day"
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(HowManyDays.rightAnswer), userInfo: nil, repeats: false)
    }
    
    @objc func rightAnswer(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "Great"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        if(self.brushing_teeth.alpha == 1.0){
            //llamo siguiente imagen
            _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(HowManyDays.LoadImage2), userInfo: nil, repeats: false)
        }
        else if(self.cleaning_ears.alpha == 1.0){
            //llamo siguiente imagen
            _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(HowManyDays.LoadImage3), userInfo: nil, repeats: false)
        }
        else if(self.taking_shower.alpha == 1.0){
            //llamo siguiente imagen
            _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(HowManyDays.LoadImage4), userInfo: nil, repeats: false)
        }
        else if(self.eating_banana.alpha == 1.0){
            //llamo siguiente imagen
            _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(HowManyDays.LoadImage5), userInfo: nil, repeats: false)
        }
        else if(self.removing_pulgas.alpha == 1.0){
            //Función animación Estrellita
            _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(HowManyDays.animationStar), userInfo: nil, repeats: false)
        }
    }
    
    @IBAction func ActionBack(_ sender: AnyObject) {
        timer.invalidate()
        if(activityPlayer.isPlaying){
            activityPlayer.stop()
        }
        pausa = true
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "MenuOpenView")
        self.present(fvc!, animated: false, completion: nil)
    }

    @objc func animationStar(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "thanks-you-have-completed-this-activity"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        /*star = UIImageView()
        star.image = UIImage(named: "star.png")
        star.frame = CGRect(x: 300, y: 764, width: 100, height: 100)
        star.alpha = 0.7
        view.addSubview(star)
        UIView.animateWithDuration(2.0, animations: {
        self.star.frame = CGRect(x: 300+60, y: 764-204, width: 84, height: 78)
        self.star.alpha = 1.0
        })
        UIView.animateWithDuration(4.0, animations: {
        self.star.frame = CGRect(x: 360+10, y: 560-440, width: 84, height: 78)
        self.star.alpha = 1.0
        })
        UIView.animateWithDuration(6.0, animations: {
        self.star.frame = CGRect(x: 600+10, y: 120-70, width: 84, height: 78)
        self.star.alpha = 1.0
        })*/
        
        star = UIImageView()
        star.image = UIImage(named: "star.png")
        star.frame = CGRect(x: 200, y: 800, width: 100, height: 100)
        self.view.addSubview(star)
        
        // randomly create a value between 0.0 and 150.0
        _ = CGFloat( arc4random_uniform(150))
        
        // for every y-value on the bezier curve
        // add our random y offset so that each individual animation
        // will appear at a different y-position
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 200,y: 800))
        path.addCurve(to: CGPoint(x: 810, y: -130), controlPoint1: CGPoint(x: 536, y: 473), controlPoint2: CGPoint(x: 778, y: 40))
        
        // create the animation
        let anim = CAKeyframeAnimation(keyPath: "position")
        anim.path = path.cgPath
        anim.rotationMode = CAAnimationRotationMode.rotateAuto
        anim.repeatCount = 1
        anim.duration = 2.0
        
        // add the animation
        star.layer.add(anim, forKey: "animate position along path")
        
        let filePath = Bundle.main.path(forResource: "sfx_star", ofType: "mp3")
        let fileURL = URL(fileURLWithPath: filePath!)
        var soundID:SystemSoundID = 0
        AudioServicesCreateSystemSoundID(fileURL as CFURL, &soundID)
        AudioServicesPlaySystemSound(soundID)
        
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(HowManyDays.setAccionMenu), userInfo: nil, repeats: false)
        
    }
    
    @objc func setAccionMenu(){
        star.alpha = 0.0
        Userstars = Userstars + 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        debugPrint("Activiry 4 deinitialized...")
    }

}

//
//  DaysOfTheWeekActivityViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 17/03/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

var activityPlayer = AVAudioPlayer()
var pausa : Bool!
var activity_name : NSString!

class DaysOfTheWeekViewController: UIViewController {
    @IBOutlet weak var btn_monday : UIButton!
    @IBOutlet weak var btn_tuesday : UIButton!
    @IBOutlet weak var btn_wednesday : UIButton!
    @IBOutlet weak var btn_thursday : UIButton!
    @IBOutlet weak var btn_friday : UIButton!
    @IBOutlet weak var btn_saturday : UIButton!
    @IBOutlet weak var btn_sunday : UIButton!
    @IBOutlet weak var monkey_intro : UIImageView!
    @IBOutlet weak var hightlight_wednesday : UIImageView!
    @IBOutlet weak var hightlight_sunday : UIImageView!
    @IBOutlet weak var big_wednesday : UIImageView!
    @IBOutlet weak var big_sunday : UIImageView!
    @IBOutlet weak var btn_one : UIButton!
    @IBOutlet weak var btn_two : UIButton!
    @IBOutlet weak var btn_three : UIButton!
    @IBOutlet weak var btn_four : UIButton!
    @IBOutlet weak var btn_five : UIButton!
    @IBOutlet weak var btn_six: UIButton!
    @IBOutlet weak var btn_seven: UIButton!
    @IBOutlet weak var btn_info: UIButton!
    
    var star : UIImageView!
    var timer : Timer!
    
    override func viewDidAppear(_ animated: Bool) {
        // Removed deprecated use of AVAudioSessionDelegate protocol
        //AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
        //AVAudioSession.sharedInstance().setActive(true, error: nil)
        //accionMenu = ""
        pausa = false
        btn_info.isSelected = false
        accionMenu = ""
        activity_name = "activityA"
        //Creamos un timer para llamar a la animación de cargar Lunes
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(DaysOfTheWeekViewController.loadIntermediate), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionInfo(_ sender: AnyObject) {
        timer.invalidate()
        if(activityPlayer.isPlaying){
            activityPlayer.stop()
        }
        pausa = true
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "DaysOfTheWeekView")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    @objc func loadIntro(){
        let sonido_activity = "this-are-the7days"
        
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.monkey_intro.alpha = 0.0
            }, completion: nil)
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        let _:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        if(pausa == false){
            timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(DaysOfTheWeekViewController.loadMonday), userInfo: nil, repeats: false)
        }
    }
    
    
    @IBAction func ActionSoundMonday(_ sender: AnyObject) {
        soundMonday()
    }
    
    @IBAction func ActionSoundTuesday(_ sender: AnyObject) {
        soundTuesday()
    }
    
    @IBAction func ActionSoundWednesday(_ sender: AnyObject) {
        soundWednesday()
    }
    
    @IBAction func ActionSoundThursday(_ sender: AnyObject) {
        soundThursday()
    }
    
    @IBAction func ActionSoundFriday(_ sender: AnyObject) {
        soundFriday()
    }
    
    @IBAction func ActionSoundSaturday(_ sender: AnyObject) {
        soundSaturday()
    }
    
    @IBAction func ActionSoundSunday(_ sender: AnyObject) {
        soundSunday()
    }
    
    func soundMonday(){
        if(pausa == false){
            let sonido_activity = "Monday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            // var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                // error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    @objc func loadMonday(){
        soundMonday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.btn_monday.alpha = 1.0
            }, completion: { finished in self.loadTuesday()
        })
    }
    
    func soundTuesday(){
        if(pausa == false){
            let sonido_activity = "Tuesday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            // var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                // error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadTuesday(){
        soundTuesday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.btn_tuesday.alpha = 1.0
            }, completion: { finished in self.loadWednesday()
        })
    }
    
    func soundWednesday(){
        if(pausa == false){
            let sonido_activity = "Wednesday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            // var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                // error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadWednesday(){
        soundWednesday()
        UIView.animate(withDuration: 1.2, delay: 0.0, options: .curveEaseOut, animations: {
            self.btn_wednesday.alpha = 1.0
            self.big_wednesday.alpha = 1.0
            }, completion: { finished in self.loadThursday()
        })
    }
    
    func soundThursday(){
        if(pausa == false){
            let sonido_activity = "Thursday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            // var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                // error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadThursday(){
        soundThursday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.btn_thursday.alpha = 1.0
            }, completion: { finished in self.loadFriday()
        })
    }
    
    func soundFriday(){
        if(pausa == false){
            let sonido_activity = "Friday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            // var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadFriday(){
        soundFriday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.btn_friday.alpha = 1.0
            }, completion: { finished in self.loadSaturday()
        })
    }
    
    func soundSaturday(){
        if(pausa == false){
            let sonido_activity = "Saturday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            // var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadSaturday(){
        soundSaturday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.btn_saturday.alpha = 1.0
            }, completion: { finished in self.loadSunday()
        })
    }
    
    func soundSunday(){
        if(pausa == false){
            let sonido_activity = "Sunday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            // var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadSunday(){
        soundSunday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            self.btn_sunday.alpha = 1.0
            self.big_sunday.alpha = 1.0
            }, completion: { finished in self.loadQuestion()
        })
    }
    
    @objc func loadIntermediate(){
        if(pausa == false){
            let sonido_activity = "is-monkey-counting-correct"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            // var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                // error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
            
            UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
                self.monkey_intro.alpha = 1.0
                }, completion: nil)
            
            timer = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(DaysOfTheWeekViewController.loadIntro), userInfo: nil, repeats: false)
        }
    }
    
    func loadQuestion(){
        let sonido_activity = "how-many-days-have-passed"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        UIView.animate(withDuration: 1.2, delay: 2.0, options: .curveEaseOut, animations: {
            self.hightlight_wednesday.frame = CGRect(x: 306, y: 0, width: 129, height: 285)
            self.big_wednesday.frame = CGRect(x: 290, y: 284, width: 161, height: 176)
            }, completion: { finished in self.loadHighlightB()
        })
    }
    
    func loadHighlightB(){
        UIView.animate(withDuration: 1.0, delay: 1.0, options: .curveEaseOut, animations: {
            self.hightlight_sunday.frame = CGRect(x: 869, y: 0, width: 139, height: 285)
            self.big_sunday.frame = CGRect(x: 859, y: 284, width: 170, height: 179)
            }, completion: nil)
        
        if(btn_info.isSelected == false){
            timer = Timer.scheduledTimer(timeInterval: 6.0, target: self, selector: #selector(DaysOfTheWeekViewController.loadAnswer), userInfo: nil, repeats: false)
        }
        else{
            timer = Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(DaysOfTheWeekViewController.loadAnswer), userInfo: nil, repeats: false)
        }
    }
    
    @objc func loadAnswer(){
        let sonido_activity = "touch-the-number"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
         timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(DaysOfTheWeekViewController.loadNumbers), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionOne(_ sender: AnyObject) {
        let sonido_activity = "1-day"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DaysOfTheWeekViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionTwo(_ sender: AnyObject) {
        let sonido_activity = "2-days"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DaysOfTheWeekViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionThree(_ sender: AnyObject) {
        let sonido_activity = "3-days"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DaysOfTheWeekViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionFour(_ sender: AnyObject) {
        let sonido_activity = "4-days"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DaysOfTheWeekViewController.rightAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionFive(_ sender: AnyObject) {
        let sonido_activity = "5-days"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DaysOfTheWeekViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionSix(_ sender: AnyObject) {
        let sonido_activity = "6-days"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DaysOfTheWeekViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    
    @IBAction func ActionSeven(_ sender: AnyObject) {
        let sonido_activity = "7-days"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DaysOfTheWeekViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionBack(_ sender: AnyObject) {
        timer.invalidate()
        if(activityPlayer.isPlaying){
            activityPlayer.stop()
        }
        pausa = true
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "MenuOpenView")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    @objc func loadNumbers(){
        UIView.animate(withDuration: 1.0, delay: 0.0, options: .curveEaseOut, animations: {
            self.btn_one.alpha = 1.0
            self.btn_two.alpha = 1.0
            self.btn_three.alpha = 1.0
            self.btn_four.alpha = 1.0
            self.btn_five.alpha = 1.0
            self.btn_six.alpha = 1.0
            self.btn_seven.alpha = 1.0
            }, completion: nil)
    }
    
    @objc func rightAnswer(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "Great"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        //Función animación Estrellita
        timer = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(DaysOfTheWeekViewController.animationStar), userInfo: nil, repeats: false)
    }
    
    @objc func badAnswer(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "please-try-again"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    @objc func animationStar(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "thanks-you-have-completed-this-activity"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        /*star = UIImageView()
        star.image = UIImage(named: "star.png")
        star.frame = CGRect(x: 300, y: 764, width: 100, height: 100)
        star.alpha = 0.7
        view.addSubview(star)
        UIView.animateWithDuration(2.0, animations: {
            self.star.frame = CGRect(x: 300+60, y: 764-204, width: 84, height: 78)
            self.star.alpha = 1.0
        })
        UIView.animateWithDuration(4.0, animations: {
            self.star.frame = CGRect(x: 360+10, y: 560-440, width: 84, height: 78)
            self.star.alpha = 1.0
        })
        UIView.animateWithDuration(6.0, animations: {
            self.star.frame = CGRect(x: 600+10, y: 120-70, width: 84, height: 78)
            self.star.alpha = 1.0
        })*/
        
        star = UIImageView()
        star.image = UIImage(named: "star.png")
        star.frame = CGRect(x: 200, y: 800, width: 100, height: 100)
        self.view.addSubview(star)
        
        // randomly create a value between 0.0 and 150.0
        _ = CGFloat( arc4random_uniform(150))
        
        // for every y-value on the bezier curve
        // add our random y offset so that each individual animation
        // will appear at a different y-position
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 200,y: 800))
        path.addCurve(to: CGPoint(x: 810, y: -130), controlPoint1: CGPoint(x: 536, y: 473), controlPoint2: CGPoint(x: 778, y: 40))
        
        // create the animation
        let anim = CAKeyframeAnimation(keyPath: "position")
        anim.path = path.cgPath
        anim.rotationMode = CAAnimationRotationMode.rotateAuto
        anim.repeatCount = 1
        anim.duration = 2.0
        
        // add the animation
        star.layer.add(anim, forKey: "animate position along path")
        
        let filePath = Bundle.main.path(forResource: "sfx_star", ofType: "mp3")
        let fileURL = URL(fileURLWithPath: filePath!)
        var soundID:SystemSoundID = 0
        AudioServicesCreateSystemSoundID(fileURL as CFURL, &soundID)
        AudioServicesPlaySystemSound(soundID)
        
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(DaysOfTheWeekViewController.setAccionMenu), userInfo: nil, repeats: false)
        
    }
    
    @objc func setAccionMenu(){
        star.alpha = 0.0
        Userstars = Userstars + 1
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }

}

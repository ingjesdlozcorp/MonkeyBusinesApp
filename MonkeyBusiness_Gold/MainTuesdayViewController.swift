//
//  MainTuesdayViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 25/03/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//


import UIKit
import AVFoundation
import MediaPlayer

class MainTuesdayViewController: UIViewController, UIPageViewControllerDataSource{
    //weak var delegate : PlayerDelegate?
    
    // MARK: - Variables
    fileprivate var pageViewController: UIPageViewController?
    
    // Initialize it scrollimages
    fileprivate let contentImages = ["",
        "",
        "",
        "",
        "",
        "",
        "",
        ""];
    
    // Initialize it right away here
    fileprivate let centerImages = ["3-1A.png",
        "3-1B.png",
        "3-2A.png",
        "3-2B.png",
        "3-2C.png",
        "3-3.png",
        "3-4.png",
        "3-5.png"];
    
    
    // Initialize Labels Text
    fileprivate let textLabels = ["With pen and paper in hand Monkey Louie asks Cheetah: “what day of the week did your food disappear?” —“last Tuesday” Cheetah says-",
        "“I remember it well because Tuesday is my favorite day, I get double steaks for dinner that day”",
        "Then Monkey Louie asks: “tell me Cheetah, what’s your routine on Tuesdays?”",
        "and Cheetah replies: “I run in the morning, I run in the afternoon, and I run in the evening,",
        "not to brag about it, but you know, I’m the fastest running animal on earth!”",
        "“I should mention that day I did notice something unusual” Cheetah adds, “after my food disappeared, I followed a tail around the bushes, but then it was gone so I couldn’t say what animal’s tail it was.”",
        "Monkey Louie says: “Maybe you were running in circles following your own tail Cheetah”",
        "Cheetah didn’t seem to like Monkey Louie’s joke and replies “That’s not funny, maybe you want me to run after you, I wouldn’t mind having monkey for dinner”"];
    
    // Initialize it with sounds
    fileprivate let soundText = ["3.1A",
        "3.1B",
        "3.2A",
        "3.2B1",
        "3.2B2",
        "3.3",
        "3.4",
        "3.5"];
    
    var timer = Timer()
    
    // MARK: - View Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        createPageViewController()
        setupPageControl()
        audiotermino = false
        contador = 0
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(MainTuesdayViewController.AudioFinished), userInfo: nil, repeats: true)
    }
    
    fileprivate func createPageViewController() {
        let pageController = self.storyboard!.instantiateViewController(withIdentifier: "PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if contentImages.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChild(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
    }
    
    fileprivate func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.backgroundColor = UIColor.clear
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.white
    }
    
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex > 0 {
            ultimo = false
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex+1 < contentImages.count {
            return getItemController(itemController.itemIndex+1)
        }
        else if itemController.itemIndex+1 == contentImages.count{
            ultimo = true
        }
        
        return nil
    }

    
    fileprivate func getItemController(_ itemIndex: Int) -> PageItemController? {
        
        if itemIndex < contentImages.count {
            
            let pageItemController = self.storyboard!.instantiateViewController(withIdentifier: "ItemController") as! PageItemController
            
            pageItemController.itemIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.imageCenter = centerImages[itemIndex]
            pageItemController.ReadText = textLabels[itemIndex]
            pageItemController.audioText = soundText[itemIndex]
            pageItemController.AppearText = textLabels[itemIndex]
            
            actual_index = itemIndex
            
            return pageItemController
        }
        
        return nil
    }
    
    @objc func AudioFinished(){
        if(historyPlayer.isPlaying == false){
            contador += 1
        }
        if(contador>=6 && type_of_read == "autoplay" && standby == false){
            contador = 0
            if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![0])!) {
                
                pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                
                _ = self.children.last
            }
            if(ultimo==true){
                day = "Wednesday"
                let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
                self.present(fvc!, animated: false, completion: nil)
                timer.invalidate()
            }
        }
        
    }
    
    @IBAction func ActionNext(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        }
    }
    
    @IBAction func ActionPrev(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
        }
    }
    
    // MARK: - Page Indicator
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return contentImages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }

}

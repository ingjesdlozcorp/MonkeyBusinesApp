//
//  BeforeAndAfter.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 14/04/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

var hard : Bool = Bool()

class BeforeAndAfter: UIViewController {
    @IBOutlet weak var btn_monday : UIButton!
    @IBOutlet weak var btn_tuesday : UIButton!
    @IBOutlet weak var btn_wednesday : UIButton!
    @IBOutlet weak var btn_thursday : UIButton!
    @IBOutlet weak var btn_friday : UIButton!
    @IBOutlet weak var btn_saturday : UIButton!
    @IBOutlet weak var btn_sunday : UIButton!
    @IBOutlet weak var hightlight_friday : UIImageView!
    @IBOutlet weak var hightlight_tuesday : UIImageView!
    @IBOutlet weak var hightlight_thursday : UIImageView!
    @IBOutlet weak var hightlight_wednesday: UIImageView!
    @IBOutlet weak var btn_hard: UIButton!
    @IBOutlet weak var btn_easy: UIButton!
    @IBOutlet weak var bg_btn_hard : UIImageView!
    @IBOutlet weak var bg_btn_easy : UIImageView!
    
    @IBOutlet weak var btn_monday_hard : UIButton!
    @IBOutlet weak var btn_tuesday_hard : UIButton!
    @IBOutlet weak var btn_wednesday_hard : UIButton!
    @IBOutlet weak var btn_thursday_hard : UIButton!
    @IBOutlet weak var btn_friday_hard : UIButton!
    @IBOutlet weak var btn_saturday_hard : UIButton!
    @IBOutlet weak var btn_sunday_hard : UIButton!
    @IBOutlet weak var hightlight_friday_hard : UIImageView!
    @IBOutlet weak var hightlight_tuesday_hard : UIImageView!
    @IBOutlet weak var hightlight_thursday_hard : UIImageView!
    @IBOutlet weak var hightlight_wednesday_hard: UIImageView!
    
    @IBOutlet weak var question: UILabel!
    @IBOutlet weak var answer: UILabel!
    
    @IBOutlet weak var hightlight_hand_friday: UIImageView!
    @IBOutlet weak var hightlight_hand_tuesday: UIImageView!
    @IBOutlet weak var hightlight_hand_thursday: UIImageView!
    @IBOutlet weak var hightlight_hand_wednesday: UIImageView!
    @IBOutlet weak var hightlight_hand_friday_easy: UIImageView!
    @IBOutlet weak var hightlight_hand_tuesday_easy: UIImageView!
    @IBOutlet weak var hightlight_hand_thursday_easy: UIImageView!
    @IBOutlet weak var hightlight_hand_wednesday_easy: UIImageView!
    @IBOutlet weak var btn_info: UIButton!
    
    var star : UIImageView!
    var timer : Timer!
    
    override func viewDidAppear(_ animated: Bool) {
        //super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        self.btn_easy.isSelected = false
        self.btn_hard.isSelected = true
        accionMenu = ""
        activity_name = "activityE"
        pausa = false
        
        //Creamos un timer para llamar a la animación de cargar Lunes
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(BeforeAndAfter.loadElements), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionBtnInfo(_ sender: AnyObject) {
        timer.invalidate()
        if(activityPlayer.isPlaying){
            activityPlayer.stop()
        }
        pausa = true
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "BeforeAndAfter")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    @objc func loadElements(){
        UIView.animate(withDuration: 6.0, delay: 0.0, options: .curveEaseOut, animations: {
            let sonido_activity = "this-are-the7days"
            
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
            
            }, completion: { finished in self.delayMonday()
        })
    }
    
    func delayMonday(){
        if(pausa == false){
            _ = Timer.scheduledTimer(timeInterval: 3.5, target: self, selector: #selector(BeforeAndAfter.loadMonday), userInfo: nil, repeats: false)
        }
    }
    
    func soundMonday(){
        if(pausa == false){
            let sonido_activity = "Monday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    @objc func loadMonday(){
        soundMonday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            if(self.btn_easy.isSelected == true){
                self.btn_monday.alpha = 1.0
            }
            if(self.btn_hard.isSelected == true){
                self.btn_monday_hard.alpha = 1.0
            }
            }, completion: { finished in self.loadTuesday()
        })
    }
    
    func soundTuesday(){
        if(pausa == false){
            let sonido_activity = "Tuesday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadTuesday(){
        soundTuesday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            if(self.btn_easy.isSelected == true){
                self.btn_tuesday.alpha = 1.0
            }
            if(self.btn_hard.isSelected == true){
                self.btn_tuesday_hard.alpha = 1.0
            }
            }, completion: { finished in self.loadWednesday()
        })
    }
    
    func soundWednesday(){
        if(pausa == false){
            let sonido_activity = "Wednesday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadWednesday(){
        soundWednesday()
        UIView.animate(withDuration: 1.2, delay: 0.0, options: .curveEaseOut, animations: {
            if(self.btn_easy.isSelected == true){
                self.btn_wednesday.alpha = 1.0
            }
            if(self.btn_hard.isSelected == true){
                self.btn_wednesday_hard.alpha = 1.0
            }
            }, completion: { finished in self.loadThursday()
        })
    }
    
    func soundThursday(){
        if(pausa == false){
            let sonido_activity = "Thursday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadThursday(){
        soundThursday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            if(self.btn_easy.isSelected == true){
                self.btn_thursday.alpha = 1.0
            }
            if(self.btn_hard.isSelected == true){
                self.btn_thursday_hard.alpha = 1.0
            }
            }, completion: { finished in self.loadFriday()
        })
    }
    
    func soundFriday(){
        if(pausa == false){
            let sonido_activity = "Friday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadFriday(){
        soundFriday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            if(self.btn_easy.isSelected == true){
                self.btn_friday.alpha = 1.0
            }
            if(self.btn_hard.isSelected == true){
                self.btn_friday_hard.alpha = 1.0
            }
            }, completion: { finished in self.loadSaturday()
        })
    }
    
    func soundSaturday(){
        if(pausa == false){
            let sonido_activity = "Saturday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadSaturday(){
        soundSaturday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            if(self.btn_easy.isSelected == true){
                self.btn_saturday.alpha = 1.0
            }
            if(self.btn_hard.isSelected == true){
                self.btn_saturday_hard.alpha = 1.0
            }
            }, completion: { finished in self.loadSunday()
        })
    }
    
    func soundSunday(){
        if(pausa == false){
            let sonido_activity = "Sunday"
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
        
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
        }
    }
    
    func loadSunday(){
        soundSunday()
        UIView.animate(withDuration: 1.1, delay: 0.0, options: .curveEaseOut, animations: {
            if(self.btn_easy.isSelected == true){
                self.btn_sunday.alpha = 1.0
            }
            if(self.btn_hard.isSelected == true){
                self.btn_sunday_hard.alpha = 1.0
            }
            }, completion: { finished in self.loadIntro()
        })
    }
    
    @objc func loadIntro(){
        if(pausa == false){
            UIView.animate(withDuration: 2.0, delay: 0.0, options: .curveEaseOut, animations: {
                let sonido_activity = "help-monkey-find-out"
            
                let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
                //var error:NSError?
            
                do {
                    activityPlayer = try AVAudioPlayer(contentsOf: readSound)
                } catch {
                    //error = error1
                    activityPlayer = AVAudioPlayer()
                }
                activityPlayer.prepareToPlay()
                activityPlayer.play()
                }, completion: { finished in self.delay()
            })
        }
    }
    
    func delay(){
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(BeforeAndAfter.loadQuestion1), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionBtnMonday(_ sender: AnyObject){
        self.answer.text = "Monday"
        validAnswer()
    }
    
    
    @IBAction func ActionBtnTuesday(_ sender: AnyObject) {
        self.answer.text = "Tuesday"
        validAnswer()
    }
    
    
    @IBAction func ActionBtnWednesday(_ sender: AnyObject) {
        self.answer.text = "Wednesday"
        validAnswer()
    }
    
    @IBAction func ActionBtnThursday(_ sender: AnyObject) {
        self.answer.text = "Thursday"
        validAnswer()
    }
    
    @IBAction func ActionBtnFriday(_ sender: AnyObject) {
        self.answer.text = "Friday"
        validAnswer()
    }
    
    @IBAction func ActionBtnSaturday(_ sender: AnyObject) {
        self.answer.text = "Saturday"
        validAnswer()
    }
    
    @IBAction func ActionBtnSunday(_ sender: AnyObject) {
        self.answer.text = "Sunday"
        validAnswer()
    }
    
    func resethightlights(){
        self.hightlight_friday.alpha = 0.0
        self.hightlight_tuesday.alpha = 0.0
        self.hightlight_thursday.alpha = 0.0
        self.hightlight_wednesday.alpha = 0.0
        self.hightlight_friday_hard.alpha = 0.0
        self.hightlight_tuesday_hard.alpha = 0.0
        self.hightlight_thursday_hard.alpha = 0.0
        self.hightlight_wednesday_hard.alpha = 0.0
        self.hightlight_hand_friday.alpha = 0.0
        self.hightlight_hand_tuesday.alpha = 0.0
        self.hightlight_hand_thursday.alpha = 0.0
        self.hightlight_hand_wednesday.alpha = 0.0
        self.hightlight_hand_friday_easy.alpha = 0.0
        self.hightlight_hand_tuesday_easy.alpha = 0.0
        self.hightlight_hand_thursday_easy.alpha = 0.0
        self.hightlight_hand_wednesday_easy.alpha = 0.0
    }
    
    @objc func loadQuestion1(){
        resethightlights()
        self.question.text = "What day comes before Friday?"
        self.hightlight_hand_friday.frame = CGRect(x: 601, y: -393, width: 143, height: 407)
        let sonido_activity = "what-day-comes-before-friday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        if(self.btn_easy.isSelected){
            UIView.animate(withDuration: 1.2, delay: 2.0, options: .curveEaseOut, animations: {
                self.hightlight_friday.alpha = 1.0
                self.hightlight_hand_friday_easy.alpha = 1.0
                self.hightlight_hand_friday_easy.frame = CGRect(x: 601, y: -124, width: 143, height: 407)
                }, completion: nil)
        }
        if(self.btn_hard.isSelected){
            UIView.animate(withDuration: 1.2, delay: 2.0, options: .curveEaseOut, animations: {
                self.hightlight_friday_hard.alpha = 1.0
                self.hightlight_hand_friday.alpha = 1.0
                self.hightlight_hand_friday.frame = CGRect(x: 601, y: 393-479, width: 143, height: 407)
                }, completion: nil)
        }
        _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(BeforeAndAfter.loadSelection), userInfo: nil, repeats: false)
    }
    
    @objc func loadSelection(){
        if(btn_hard.isSelected == true){
            self.btn_hard.alpha = 1.0
            self.bg_btn_hard.alpha = 1.0
        }
        if(btn_easy.isSelected == true){
            self.btn_easy.alpha = 1.0
            self.bg_btn_easy.alpha = 1.0
        }
        UIView.animate(withDuration: 6.0, delay: 6.0, options: .curveEaseOut, animations: {
            let sonido_activity = "select-lineal-order-or-split-order"
            
            let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
            //var error:NSError?
            
            do {
                activityPlayer = try AVAudioPlayer(contentsOf: readSound)
            } catch {
                //error = error1
                activityPlayer = AVAudioPlayer()
            }
            activityPlayer.prepareToPlay()
            activityPlayer.play()
            }, completion: nil)
    }
    
    @objc func loadQuestion2(){
        self.question.text = "What day comes before Tuesday?"
        self.hightlight_hand_tuesday.frame = CGRect(x: 218, y: -407, width: 143, height: 407)
        let sonido_activity = "what-day-comes-before-tuesday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        if(self.btn_easy.isSelected == true){
            UIView.animate(withDuration: 1.2, delay: 2.0, options: .curveEaseOut, animations: {
                self.hightlight_tuesday.alpha = 1.0
                self.hightlight_hand_tuesday_easy.alpha = 1.0
                self.hightlight_hand_tuesday_easy.frame = CGRect(x: 149, y: -124, width: 143, height: 407)
            }, completion: nil)
        }
        if(self.btn_hard.isSelected == true){
            UIView.animate(withDuration: 1.2, delay: 2.0, options: .curveEaseOut, animations: {
                self.hightlight_tuesday_hard.alpha = 1.0
                self.hightlight_hand_tuesday.alpha = 1.0
                self.hightlight_hand_tuesday.frame = CGRect(x: 218, y: -1, width: 143, height: 407)
            }, completion: nil)
        }
    }
    
    @objc func loadQuestion3(){
        self.question.text = "What day comes after Wednesday?"
        self.hightlight_hand_wednesday.frame = CGRect(x: 348, y: -407, width: 143, height: 407)
        let sonido_activity = "what-day-comes-after-wednesday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        if(self.btn_easy.isSelected == true){
            UIView.animate(withDuration: 1.2, delay: 2.0, options: .curveEaseOut, animations: {
                self.hightlight_wednesday.alpha = 1.0
                self.hightlight_hand_wednesday_easy.alpha = 1.0
                self.hightlight_hand_wednesday.frame = CGRect(x: 297, y: -124, width: 143, height: 407)
            }, completion: nil)
        }
        if(self.btn_hard.isSelected == true){
            UIView.animate(withDuration: 1.2, delay: 2.0, options: .curveEaseOut, animations: {
                self.hightlight_wednesday_hard.alpha = 1.0
                self.hightlight_hand_wednesday.alpha = 1.0
                self.hightlight_hand_wednesday.frame = CGRect(x: 348, y: -255, width: 143, height: 407)
            }, completion: nil)
        }
    }
    
    @objc func loadQuestion4(){
        self.question.text = "What day comes after Friday?"
        self.hightlight_hand_friday.frame = CGRect(x: 601, y: -393, width: 143, height: 407)
        let sonido_activity = "what-day-comes-after-friday"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        if(self.btn_easy.isSelected == true){
            UIView.animate(withDuration: 1.2, delay: 2.0, options: .curveEaseOut, animations: {
                self.hightlight_friday.alpha = 1.0
                self.hightlight_hand_friday_easy.alpha = 1.0
                self.hightlight_hand_friday_easy.frame = CGRect(x: 601, y: -124, width: 143, height: 407)
            }, completion: nil)
        }
        if(self.btn_hard.isSelected == true){
            UIView.animate(withDuration: 1.2, delay: 2.0, options: .curveEaseOut, animations: {
                self.hightlight_friday_hard.alpha = 1.0
                self.hightlight_hand_friday.alpha = 1.0
                self.hightlight_hand_friday.frame = CGRect(x: 601, y: 393-479, width: 143, height: 407)
            }, completion: nil)
        }
    }
    
    func validAnswer(){
        if (self.question.text == "What day comes before Friday?"){
            if(self.answer.text == "Thursday"){
                soundThursday()
                _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(BeforeAndAfter.rightAnswer), userInfo: nil, repeats: false)
                if(self.btn_easy.isSelected == true){
                    self.hightlight_friday.alpha = 0.0
                    self.hightlight_hand_friday_easy.alpha = 0.0
                }
                if(self.btn_hard.isSelected == true){
                    self.hightlight_friday_hard.alpha = 0.0
                    self.hightlight_hand_friday.alpha = 0.0
                }
                _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(BeforeAndAfter.loadQuestion2), userInfo: nil, repeats: false)
            }
            else{
                if(self.answer.text == "Monday"){
                    soundMonday()
                }
                else if(self.answer.text == "Tuesday"){
                    soundTuesday()
                }
                else if(self.answer.text == "Wednesday"){
                    soundWednesday()
                }
                else if(self.answer.text == "Friday"){
                    soundFriday()
                }
                else if(self.answer.text == "Saturday"){
                    soundSaturday()
                }
                else if(self.answer.text == "Sunday"){
                    soundSunday()
                }
                _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(BeforeAndAfter.badAnswer), userInfo: nil, repeats: false)
            }
        }
        if (self.question.text == "What day comes before Tuesday?"){
            if(self.answer.text == "Monday"){
                soundMonday()
                _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(BeforeAndAfter.rightAnswer), userInfo: nil, repeats: false)
                if(self.btn_easy.isSelected == true){
                    self.hightlight_tuesday.alpha = 0.0
                    self.hightlight_hand_tuesday_easy.alpha = 0.0
                }
                if(self.btn_hard.isSelected == true){
                    self.hightlight_tuesday_hard.alpha = 0.0
                    self.hightlight_hand_tuesday.alpha = 0.0
                }
                _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(BeforeAndAfter.loadQuestion3), userInfo: nil, repeats: false)
            }
            else{
                if(self.answer.text == "Thursday"){
                    soundThursday()
                }
                else if(self.answer.text == "Tuesday"){
                    soundTuesday()
                }
                else if(self.answer.text == "Wednesday"){
                    soundWednesday()
                }
                else if(self.answer.text == "Friday"){
                    soundFriday()
                }
                else if(self.answer.text == "Saturday"){
                    soundSaturday()
                }
                else if(self.answer.text == "Sunday"){
                    soundSunday()
                }
                _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(BeforeAndAfter.badAnswer), userInfo: nil, repeats: false)
            }
        }
        if (self.question.text == "What day comes after Wednesday?"){
            if(self.answer.text == "Thursday"){
                soundThursday()
                _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(BeforeAndAfter.rightAnswer), userInfo: nil, repeats: false)
                if(self.btn_easy.isSelected == true){
                    self.hightlight_wednesday.alpha = 0.0
                    self.hightlight_hand_wednesday_easy.alpha = 0.0
                }
                if(self.btn_hard.isSelected == true){
                    self.hightlight_wednesday_hard.alpha = 0.0
                    self.hightlight_hand_wednesday.alpha = 0.0
                }
                _ = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(BeforeAndAfter.loadQuestion4), userInfo: nil, repeats: false)
            }
            else{
                if(self.answer.text == "Monday"){
                    soundMonday()
                }
                else if(self.answer.text == "Tuesday"){
                    soundTuesday()
                }
                else if(self.answer.text == "Wednesday"){
                    soundWednesday()
                }
                else if(self.answer.text == "Friday"){
                    soundFriday()
                }
                else if(self.answer.text == "Saturday"){
                    soundSaturday()
                }
                else if(self.answer.text == "Sunday"){
                    soundSunday()
                }
                _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(BeforeAndAfter.badAnswer), userInfo: nil, repeats: false)
            }
        }
        if (self.question.text == "What day comes after Friday?"){
            if(self.answer.text == "Saturday"){
                soundSaturday()
                _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(BeforeAndAfter.endActivity), userInfo: nil, repeats: false)
                if(self.btn_easy.isSelected == true){
                    self.hightlight_friday.alpha = 0.0
                    self.hightlight_hand_friday_easy.alpha = 0.0
                }
                if(self.btn_hard.isSelected == true){
                    self.hightlight_friday_hard.alpha = 0.0
                    self.hightlight_hand_friday.alpha = 0.0
                }
                self.question.text = ""
            }
            else{
                if(self.answer.text == "Monday"){
                    soundMonday()
                }
                else if(self.answer.text == "Tuesday"){
                    soundTuesday()
                }
                else if(self.answer.text == "Wednesday"){
                    soundWednesday()
                }
                else if(self.answer.text == "Friday"){
                    soundFriday()
                }
                else if(self.answer.text == "Thursday"){
                    soundThursday()
                }
                else if(self.answer.text == "Sunday"){
                    soundSunday()
                }
                _ = Timer.scheduledTimer(timeInterval: 1.5, target: self, selector: #selector(BeforeAndAfter.badAnswer), userInfo: nil, repeats: false)
            }
        }
    }
    
    @objc func rightAnswer(){
        let sonido_activity = "Great"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    @objc func endActivity(){
        //Función animación Estrellita
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(BeforeAndAfter.animationStar), userInfo: nil, repeats: false)
    }
    
    @objc func badAnswer(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "please-try-again"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    @IBAction func ActionBack(_ sender: AnyObject) {
        // TODO
        // timer.invalidate()
        if(activityPlayer.isPlaying){
            activityPlayer.stop()
        }
        pausa = true
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "MenuOpenView")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    @objc func animationStar(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "thanks-you-have-completed-this-activity"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        //var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            //error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        /*star = UIImageView()
        star.image = UIImage(named: "star.png")
        star.frame = CGRect(x: 300, y: 764, width: 100, height: 100)
        star.alpha = 0.7
        view.addSubview(star)
        UIView.animateWithDuration(2.0, animations: {
        self.star.frame = CGRect(x: 300+60, y: 764-204, width: 84, height: 78)
        self.star.alpha = 1.0
        })
        UIView.animateWithDuration(4.0, animations: {
        self.star.frame = CGRect(x: 360+10, y: 560-440, width: 84, height: 78)
        self.star.alpha = 1.0
        })
        UIView.animateWithDuration(6.0, animations: {
        self.star.frame = CGRect(x: 600+10, y: 120-70, width: 84, height: 78)
        self.star.alpha = 1.0
        })*/
        
        star = UIImageView()
        star.image = UIImage(named: "star.png")
        star.frame = CGRect(x: 200, y: 800, width: 100, height: 100)
        self.view.addSubview(star)
        
        // randomly create a value between 0.0 and 150.0
        _ = CGFloat( arc4random_uniform(150))
        
        // for every y-value on the bezier curve
        // add our random y offset so that each individual animation
        // will appear at a different y-position
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 200,y: 800))
        path.addCurve(to: CGPoint(x: 810, y: -130), controlPoint1: CGPoint(x: 536, y: 473), controlPoint2: CGPoint(x: 778, y: 40))
        
        // create the animation
        let anim = CAKeyframeAnimation(keyPath: "position")
        anim.path = path.cgPath
        anim.rotationMode = CAAnimationRotationMode.rotateAuto
        anim.repeatCount = 1
        anim.duration = 2.0
        
        
        let sound_effect = "sfx_star"
        let newsound = URL(fileURLWithPath: Bundle.main.path(forResource: sound_effect, ofType: "mp3")!)
        
        var effectPlayer = AVAudioPlayer()
        do {
            effectPlayer = try AVAudioPlayer(contentsOf: newsound)
        } catch {
            //error = error1
            effectPlayer = AVAudioPlayer()
        }
        effectPlayer.prepareToPlay()
        effectPlayer.volume = 0.5
        effectPlayer.play()

        // add the animation
        star.layer.add(anim, forKey: "animate position along path")
        
        let filePath = Bundle.main.path(forResource: "sfx_star", ofType: "mp3")
        let fileURL = URL(fileURLWithPath: filePath!)
        var soundID:SystemSoundID = 0
        AudioServicesCreateSystemSoundID(fileURL as CFURL, &soundID)
        AudioServicesPlaySystemSound(soundID)
        
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(BeforeAndAfter.setAccionMenu), userInfo: nil, repeats: false)
        
    }
    
    @objc func setAccionMenu(){
        star.alpha = 0.0
        Userstars = Userstars + 1
    }
    
    @IBAction func ActionHard(_ sender: AnyObject) {
        self.bg_btn_hard.alpha = 0.0
        self.btn_hard.alpha = 0.0
        self.bg_btn_easy.alpha = 1.0
        self.btn_easy.alpha = 1.0
        self.btn_hard.isSelected = false
        self.btn_easy.isSelected = true
        resethightlights()
        self.btn_monday.alpha = 1.0
        self.btn_tuesday.alpha = 1.0
        self.btn_wednesday.alpha = 1.0
        self.btn_thursday.alpha = 1.0
        self.btn_friday.alpha = 1.0
        self.btn_saturday.alpha = 1.0
        self.btn_sunday.alpha = 1.0
        self.btn_monday_hard.alpha = 0.0
        self.btn_tuesday_hard.alpha = 0.0
        self.btn_wednesday_hard.alpha = 0.0
        self.btn_thursday_hard.alpha = 0.0
        self.btn_friday_hard.alpha = 0.0
        self.btn_saturday_hard.alpha = 0.0
        self.btn_sunday_hard.alpha = 0.0
        
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(BeforeAndAfter.loadIntro), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionEasy(_ sender: AnyObject) {
        self.bg_btn_hard.alpha = 1.0
        self.btn_hard.alpha = 1.0
        self.bg_btn_easy.alpha = 0.0
        self.btn_easy.alpha = 0.0
        self.btn_easy.isSelected = false
        self.btn_hard.isSelected = true
        resethightlights()
        self.btn_monday_hard.alpha = 1.0
        self.btn_tuesday_hard.alpha = 1.0
        self.btn_wednesday_hard.alpha = 1.0
        self.btn_thursday_hard.alpha = 1.0
        self.btn_friday_hard.alpha = 1.0
        self.btn_saturday_hard.alpha = 1.0
        self.btn_sunday_hard.alpha = 1.0
        self.btn_monday.alpha = 0.0
        self.btn_tuesday.alpha = 0.0
        self.btn_wednesday.alpha = 0.0
        self.btn_thursday.alpha = 0.0
        self.btn_friday.alpha = 0.0
        self.btn_saturday.alpha = 0.0
        self.btn_sunday.alpha = 0.0
        
        _ = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(BeforeAndAfter.loadIntro), userInfo: nil, repeats: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }

}

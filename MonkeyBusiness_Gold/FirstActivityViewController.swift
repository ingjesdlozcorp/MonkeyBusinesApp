//
//  FirstActivityViewController.swift
//  MonkeyBusiness_Gold
//
//  Created by Neymar Contreras on 05/03/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class FirstActivityViewController: UIViewController {
    @IBOutlet var image_activity: UIImageView!
    @IBOutlet var map: UIImageView!
    @IBOutlet var daysLabel: UILabel!
    @IBOutlet weak var mapSlider: UISlider!
    @IBOutlet weak var Slider: UIImageView!
    @IBOutlet weak var linterna_on: UIImageView!
    @IBOutlet weak var linterna_off: UIImageView!
    @IBOutlet weak var night_left: UIImageView!
    @IBOutlet weak var btn_info: UIButton!
    @IBOutlet weak var btn_linterna: UIButton!
    @IBOutlet weak var one: UIButton!
    @IBOutlet weak var two: UIButton!
    @IBOutlet weak var three: UIButton!
    @IBOutlet weak var four: UIButton!
    @IBOutlet weak var five: UIButton!
    @IBOutlet weak var six: UIButton!
    @IBOutlet weak var seven: UIButton!
    
    var star : UIImageView!
    var timer : Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //accionMenu = ""
        // Removed deprecated use of AVAudioSessionDelegate protocol
        //AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, error: nil)
        //AVAudioSession.sharedInstance().setActive(true, error: nil)
        pausa = false
        accionMenu = ""
        activity_name = "activityB"
        linterna_off.alpha = 0.0
        night_left.alpha = 0.0
        one.alpha = 0.0
        two.alpha = 0.0
        three.alpha = 0.0
        four.alpha = 0.0
        five.alpha = 0.0
        six.alpha = 0.0
        seven.alpha = 0.0
        self.mapSlider.setThumbImage(UIImage(named: "cursor_scroll_er.png"), for: UIControl.State())
        self.mapSlider.isEnabled = false
        self.mapSlider.alpha = 0.0
        Slider.alpha = 0.0
        //Creamos un timer para llamar a la animación de cargar Lunes
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(FirstActivityViewController.loadIntro), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionInfo(_ sender: AnyObject) {
        timer.invalidate()
        if(activityPlayer.isPlaying){
            activityPlayer.stop()
        }
        pausa = true
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "FirstActivityView")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    @IBAction func BtnLinterna(_ sender: AnyObject) {
        if(linterna_off.alpha==0.0){
            linterna_off.alpha=1.0
            linterna_on.alpha=0.0
            night_left.alpha=1.0
        }
        else{
            linterna_off.alpha=0.0
            linterna_on.alpha=1.0
            night_left.alpha=0.0
        }
    }
    
    @objc func loadIntro(){
        let sonido_activity = "koala-explained-that"
            
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
            
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        if(pausa == false){
            _ = Timer.scheduledTimer(timeInterval: 7.5, target: self, selector: #selector(FirstActivityViewController.loadElements), userInfo: nil, repeats: false)
        }
    }
    
    @objc func loadElements(){
        let sonido_activity = "Monkey-Louie-Wants-to-know"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        if(pausa == false){
            _ = Timer.scheduledTimer(timeInterval: 7.5, target: self, selector: #selector(FirstActivityViewController.loadAnswer), userInfo: nil, repeats: false)
        }
    }
    
    @objc func loadAnswer(){
        let sonido_activity = "Move-the-slider"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        UIView.animate(withDuration: 2.0, delay: 1.0, options: .curveEaseOut, animations: {
            self.mapSlider.alpha = 1.0
            self.Slider.alpha = 1.0
            }, completion: nil)
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
           // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        UIView.animate(withDuration: 4.0, delay: 4.0, options: .curveEaseOut, animations: {
            self.one.alpha = 1.0
            self.two.alpha = 1.0
            self.three.alpha = 1.0
            self.four.alpha = 1.0
            self.five.alpha = 1.0
            self.six.alpha = 1.0
            self.seven.alpha = 1.0
            }, completion: { finished in self.finishTimer()
        })

    }
    
    func finishTimer(){
        self.mapSlider.isEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func mapSliderValueChanged(_ sender: UISlider) {
        let sliderValue = sender.value
        _ = UIView.AnimationOptions()
        let valor = 258 - sliderValue
        //var error:NSError?
        var sonido_activity:String?
        
        if(sliderValue <= 680){
            daysLabel.text = "SUNDAY"
            sonido_activity = "Sunday"
        }
        else if(sliderValue <= 1400){
            daysLabel.text = "MONDAY"
            sonido_activity = "Monday"
        }
        else if (sliderValue <= 2120){
            daysLabel.text = "TUESDAY"
            sonido_activity = "Tuesday"
        }
        else if (sliderValue <= 2840){
            daysLabel.text = "WEDNESDAY"
            sonido_activity = "Wednesday"
        }
        else if (sliderValue <= 3560){
            daysLabel.text = "THURSDAY"
            sonido_activity = "Thursday"
        }
        else if (sliderValue <= 4280){
            daysLabel.text = "FRIDAY"
            sonido_activity = "Friday"
        }
        else if (sliderValue <= 5000){
            daysLabel.text = "SATURDAY"
            sonido_activity = "Saturday"
        }
        
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        self.map.frame = CGRect(x: Int(valor), y: 135, width: 5040, height: 310)

    }
    
    @IBAction func ActionOneRotation(_ sender: AnyObject) {
        let sonido_activity = "1-rotation"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(FirstActivityViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionTwoRotation(_ sender: AnyObject) {
        let sonido_activity = "2-rotations"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(FirstActivityViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionThreeRotation(_ sender: AnyObject) {
        let sonido_activity = "3-rotations"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(FirstActivityViewController.rightAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionFourRotation(_ sender: AnyObject) {
        let sonido_activity = "4-rotations"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(FirstActivityViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    
    @IBAction func ActionFiveRotation(_ sender: AnyObject) {
        let sonido_activity = "5-rotations"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(FirstActivityViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionSixRotation(_ sender: AnyObject) {
        let sonido_activity = "6-rotations"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(FirstActivityViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionSevenRotation(_ sender: AnyObject) {
        let sonido_activity = "7-rotations"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(FirstActivityViewController.badAnswer), userInfo: nil, repeats: false)
    }
    
    @IBAction func ActionBack(_ sender: AnyObject) {
        timer.invalidate()
        if(activityPlayer.isPlaying){
            activityPlayer.stop()
        }
        pausa = true
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "MenuOpenView")
        self.present(fvc!, animated: false, completion: nil)
    }
    
    @objc func rightAnswer(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "Great"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        //Función animación Estrellita
        _ = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(FirstActivityViewController.animationStar), userInfo: nil, repeats: false)
    }
    
    @objc func badAnswer(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "please-try-again"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
    }
    
    @objc func animationStar(){
        if (activityPlayer.isPlaying == true){
            activityPlayer.stop()
        }
        
        let sonido_activity = "thanks-you-have-completed-this-activity"
        let readSound = URL(fileURLWithPath: Bundle.main.path(forResource: sonido_activity, ofType: "mp3")!)
        // var error:NSError?
        
        do {
            activityPlayer = try AVAudioPlayer(contentsOf: readSound)
        } catch {
            // error = error1
            activityPlayer = AVAudioPlayer()
        }
        activityPlayer.prepareToPlay()
        activityPlayer.play()
        
        /*star = UIImageView()
        star.image = UIImage(named: "star.png")
        star.frame = CGRect(x: 300, y: 764, width: 100, height: 100)
        star.alpha = 0.7
        view.addSubview(star)
        UIView.animateWithDuration(2.0, animations: {
        self.star.frame = CGRect(x: 300+60, y: 764-204, width: 84, height: 78)
        self.star.alpha = 1.0
        })
        UIView.animateWithDuration(4.0, animations: {
        self.star.frame = CGRect(x: 360+10, y: 560-440, width: 84, height: 78)
        self.star.alpha = 1.0
        })
        UIView.animateWithDuration(6.0, animations: {
        self.star.frame = CGRect(x: 600+10, y: 120-70, width: 84, height: 78)
        self.star.alpha = 1.0
        })*/
        
        star = UIImageView()
        star.image = UIImage(named: "star.png")
        star.frame = CGRect(x: 200, y: 800, width: 100, height: 100)
        self.view.addSubview(star)
        
        // randomly create a value between 0.0 and 150.0
        _ = CGFloat( arc4random_uniform(150))
        
        // for every y-value on the bezier curve
        // add our random y offset so that each individual animation
        // will appear at a different y-position
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 200,y: 800))
        path.addCurve(to: CGPoint(x: 810, y: -130), controlPoint1: CGPoint(x: 536, y: 473), controlPoint2: CGPoint(x: 778, y: 40))
        
        // create the animation
        let anim = CAKeyframeAnimation(keyPath: "position")
        anim.path = path.cgPath
        anim.rotationMode = CAAnimationRotationMode.rotateAuto
        anim.repeatCount = 1
        anim.duration = 2.0
        
        // add the animation
        star.layer.add(anim, forKey: "animate position along path")
        
        let filePath = Bundle.main.path(forResource: "sfx_star", ofType: "mp3")
        let fileURL = URL(fileURLWithPath: filePath!)
        var soundID:SystemSoundID = 0
        AudioServicesCreateSystemSoundID(fileURL as CFURL, &soundID)
        AudioServicesPlaySystemSound(soundID)
        
        _ = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(FirstActivityViewController.setAccionMenu), userInfo: nil, repeats: false)
        
    }
    
    @objc func setAccionMenu(){
        star.alpha = 0.0
        Userstars = Userstars + 1
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }
}

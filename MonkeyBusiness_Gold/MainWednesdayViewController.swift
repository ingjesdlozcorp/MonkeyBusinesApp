//
//  MainWednesdayViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 25/03/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class MainWednesdayViewController: UIViewController, UIPageViewControllerDataSource{
    //weak var delegate : PlayerDelegate?
    
    // MARK: - Variables
    fileprivate var pageViewController: UIPageViewController?
    
    // Initialize it scrollimages
    fileprivate let contentImages = ["",
        "",
        "",
        "",
        "",
        "",
        "",
        ""];
    
    // Initialize it right away here
    fileprivate let centerImages = ["4-1.png",
        "4-2.png",
        "4-3a.png",
        "4-3b.png",
        "4-4a.png",
        "4-4b.png",
        "4-5a.png",
        "4-5b.png"];
    
    
    // Initialize Labels Text
    fileprivate let textLabels = ["“Hello Croco, we meet again” —MonkeyLouie says— “please remind me what day did your food disappear?”",
        "“Ok Monkey, first of all don’t call me Croco,” Mr. Crocodile replies— “my food disappeared last Wednesday, I remember that day my friend the Blackbird Plover was helping me cleaning my teeth then I turned and realized my meal was gone”.",
        "Monkey Louie asks “...and how many times a day do your friend Blackbird cleans your teeth?”—",
        "Mr.Crocodile replies— “if you must know, it’s three times a day; first in the morning, then at noon time, and last time at night before going to sleep”",
        "“Any other curious story besides the one of having a bird cleaning your teeth?”",
        "— “well, now that you ask...” —says Crocodile — I found traces of brown fur around my food tray, actually same color as your own fur monkey”",
        "Monkey Louie replies: “Maybe you are growing fur Crocodile”",
        "— Mr. Crocodile says “That’s not funny Monkey, maybe you want to clean my teeth too?”"];
    
    // Initialize it with sounds
    fileprivate let soundText = ["4.1",
        "4.2",
        "4.3A",
        "4.3B",
        "4.4A",
        "4.4B",
        "4.5A",
        "4.5B"];
    
    var timer = Timer()
    
    // MARK: - View Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        createPageViewController()
        setupPageControl()
        audiotermino = false
        contador = 0
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(MainWednesdayViewController.AudioFinished), userInfo: nil, repeats: true)
    }
    
    fileprivate func createPageViewController() {
        let pageController = self.storyboard!.instantiateViewController(withIdentifier: "PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if contentImages.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChild(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
    }
    
    fileprivate func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.backgroundColor = UIColor.clear
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.white
    }
    
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex > 0 {
            ultimo = false
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex+1 < contentImages.count {
            return getItemController(itemController.itemIndex+1)
        }
        else if itemController.itemIndex+1 == contentImages.count{
            ultimo = true
        }
        
        return nil
    }
    
    fileprivate func getItemController(_ itemIndex: Int) -> PageItemController? {
        
        if itemIndex < contentImages.count {
            
            let pageItemController = self.storyboard!.instantiateViewController(withIdentifier: "ItemController") as! PageItemController
            
            pageItemController.itemIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.imageCenter = centerImages[itemIndex]
            pageItemController.ReadText = textLabels[itemIndex]
            pageItemController.audioText = soundText[itemIndex]
            pageItemController.AppearText = textLabels[itemIndex]
            
            actual_index = itemIndex
            
            return pageItemController
        }
        
        return nil
    }
    
    @objc func AudioFinished(){
        if(historyPlayer.isPlaying == false){
            contador += 1
        }
        if(contador>=6 && type_of_read == "autoplay" && standby == false){
            contador = 0
            if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![0])!) {
                
                pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                
                _ = self.children.last
            }
            if(ultimo==true){
                day = "Thursday"
                let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
                self.present(fvc!, animated: false, completion: nil)
                timer.invalidate()
            }
        }
        
    }
    
    @IBAction func ActionNext(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        }
    }
    
    @IBAction func ActionPrev(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
        }
    }
    
    // MARK: - Page Indicator
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return contentImages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }

    
}

//
//  ZooMapViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 31/03/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import Foundation

var day : String  = String()

class ZooMapViewController: UIViewController {
    @IBOutlet var dayDot: UIImageView!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        _ = Timer.scheduledTimer(timeInterval: 7.0, target: self, selector: #selector(ZooMapViewController.changeToHome), userInfo: nil, repeats: false)
    }
    
    @objc func changeToHome() {
        let fvc = self.storyboard?.instantiateViewController(withIdentifier: "OpenView")
        self.present(fvc!, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        debugPrint("The end deinitialized...")
    }

}

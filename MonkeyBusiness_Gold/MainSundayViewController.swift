//
//  MainViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 16/02/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

var type_of_read : String  = String()
var soundPlayer = AVAudioPlayer()
var historyPlayer = AVAudioPlayer()
var actual_index : Int = Int()
var ultimo : Bool = Bool()
var Userstars : Int = Int()
var audiotermino : Bool = Bool()
var contador: Int = Int()

class MainSundayViewController: UIViewController, UIPageViewControllerDataSource {
    
    // MARK: - Variables
    fileprivate var pageViewController: UIPageViewController?
    @IBOutlet var btn_prev: UIButton!
    @IBOutlet var btn_next: UIButton!
    
    // Initialize it scrollimages
    fileprivate let contentImages = ["",
                                 "",
                                 "",
                                 "",
                                 "",
                                 "",
                                 "",
                                 "",
                                 ""];
    
    // Initialize it right away here
    fileprivate let centerImages = ["1-1.png",
        "1-2.png",
        "1-3.png",
        "1-4A.png",
        "1-4B.png",
        "1-5.png",
        "1-6.png",
        "1-7.png",
        "1-8.png"];

    
    // Initialize Labels Text
    fileprivate let textLabels = ["It was a hot Sunday evening in the middle of summer in the Boston Zoo. A few animals were complaining about the mysterious disappearance of their food.",
        "Among roars, growls, quaks and trumpets, Mr. Crocodile asks, “Where is our food? It has been two days since my food disappeared last Wednesday”",
        "“Did you say two days?” A Monkey named Louie asks, he was eavesdropping on the animals questioning.",
        "Mr. Crocodile replies “what do you care Monkey Louie?",
        "the sassy Monkey jumps in the middle of their meeting and says; “I do care my dear friend, please allow me to review your statement...",
        "If your food disappeared last Wednesday and today is Sunday, then the correct counting should be; Thursday, Friday, Saturday and today Sunday, so it’s been four days, not two days, right?”",
        "“Alright” -says Mrs. Giraffe, “is there anything else you can do for us Monkey besides counting the days of the week?”, “Actually there is” - says Monkey Louie.",
        "“I, the Zoo greatest mind, will solve this mystery once and for all”",
        "There are mixed feelings about Monkey Louie’s proposal, then Mrs. Giraffe replies: “We don’t agree to the “Zoo greatest mind” part, however if you want to help, we’ll give 1 week to investigate”. Fair enough replies Monkey Louie - “I will solve this mystery in 7 days”."];
    
    // Initialize it with sounds
    fileprivate let soundText = ["1.1",
        "1.2",
        "1.3",
        "1.4A",
        "1.4B",
        "1.5",
        "1.6",
        "1.7",
        "1.8"];
    
    var timer = Timer()
    
    // MARK: - View Lifecycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        createPageViewController()
        setupPageControl()
        audiotermino = false
        contador = 0
        _ = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(MainSundayViewController.AudioFinished), userInfo: nil, repeats: true)
    }
    
    fileprivate func createPageViewController() {
        let pageController = self.storyboard!.instantiateViewController(withIdentifier: "PageController") as! UIPageViewController
        pageController.dataSource = self
        
        if centerImages.count > 0 {
            let firstController = getItemController(0)!
            let startingViewControllers: NSArray = [firstController]
            pageController.setViewControllers(startingViewControllers as? [UIViewController], direction: UIPageViewController.NavigationDirection.forward, animated: false, completion: nil)
        }
        
        pageViewController = pageController
        addChild(pageViewController!)
        self.view.addSubview(pageViewController!.view)
        pageViewController!.didMove(toParent: self)
    }

    fileprivate func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.backgroundColor = UIColor.clear
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.white
    }
    
    
    // MARK: - UIPageViewControllerDataSource
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex > 0 {
            ultimo = false
            return getItemController(itemController.itemIndex-1)
        }
        
        return nil
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let itemController = viewController as! PageItemController
        if itemController.itemIndex+1 < centerImages.count {
            print(itemController.itemIndex+1)
            print(centerImages.count)
            return getItemController(itemController.itemIndex+1)
        }
        else{
            ultimo = true
            day = "Monday"
            let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
            self.present(fvc!, animated: false, completion: nil)
        }
        
        return nil
    }
    
    fileprivate func getItemController(_ itemIndex: Int) -> PageItemController? {
        
        if itemIndex < centerImages.count {
            
            let pageItemController = self.storyboard!.instantiateViewController(withIdentifier: "ItemController") as! PageItemController
            
            pageItemController.itemIndex = itemIndex
            pageItemController.imageName = contentImages[itemIndex]
            pageItemController.imageCenter = centerImages[itemIndex]
            pageItemController.ReadText = textLabels[itemIndex]
            pageItemController.audioText = soundText[itemIndex]
            pageItemController.AppearText = textLabels[itemIndex]
            
            actual_index = itemIndex
            
            return pageItemController
        }
        
        return nil
    }
    
    @objc func AudioFinished(){
        if(historyPlayer.isPlaying == false){
            contador += 1
        }
        if(contador>=6 && type_of_read == "autoplay" && standby == false){
            contador = 0
            if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![0])!) {
                
                pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
                
                _ = self.children.last
            }
            if(ultimo==true){
                self.btn_next.alpha = 0
                day = "Monday"
                let fvc = self.storyboard?.instantiateViewController(withIdentifier: "IntroDay")
                self.present(fvc!, animated: false, completion: nil)
                timer.invalidate()
            }
        }
        
    }
    
    @IBAction func ActionNext(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
        }
    }
    
    @IBAction func ActionPrev(_ sender: AnyObject) {
        if let controller = pageViewController(pageViewController!, viewControllerAfter: (pageViewController?.viewControllers![actual_index])!) {
            pageViewController?.setViewControllers([controller], direction: UIPageViewController.NavigationDirection.reverse, animated: true, completion: nil)
        }
    }
    // MARK: - Page Indicator
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return centerImages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    deinit {
        debugPrint("Name_of_view_controlled deinitialized...")
    }


}


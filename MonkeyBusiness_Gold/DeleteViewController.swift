//
//  DeleteViewController.swift
//  MonkeyBusiness
//
//  Created by Neymar Contreras on 18/05/2015.
//  Copyright (c) 2015 Enrique Hurtado. All rights reserved.
//

import UIKit
import Foundation
import CoreData

class DeleteViewController: UIViewController {
    @IBOutlet weak var userText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userText.text = namePerfil as String
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool){
        let isUserLoggedIn =  UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        
        if(!isUserLoggedIn){
            userText.text = namePerfil as String
        }
        else{
            userText.text = "You are not log in!"
        }
    }
}
